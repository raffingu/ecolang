This project is dedicated to evaluate the efficiency of programming languages against one HPC use case (time vs energy)

------
Status
------

Languages included in this comparison:

| Lang | Version  | setup.sh  | upgrade (warmup + loops + timestamps) | Commands |
|---|---|---|---|---|
| C | gcc 7 / 8 | X  | X  | 1DC / 1DC_opt  |
| Fortran | gfortran 7 / 8 | X  | X  | 1Dfortran / 1Dfortran_opt |
| Julia | julia 1.4.0 | X  | X  | 1Djulia / 1Djulia_opt  |
| Java | openjdk 11.0.7 | X  | X  | 1Djava / 1Djava_opt  |
| Python | Python 3.7.6 | X  | X  | 1Dpython_numpy / 1Dpython_transonic / 1Dpython_numba  |
| rust | rust 1.43.0 | X | X  | 1Drust  |
| go | go 1.14.3  | X  | X  | 1Dgo  |
|   |   |   |   |   |


---------
How to use
---------

TODO: write shell + basic explanations to deploy & run all tests


---------
JRES 2022 Results
---------

- Energy efficiency on tested hardware infrastuctures:
![cmp hardware](/ecoconception_logicielle/postpro_scripts/ecolang_compare.png)

- Energy efficiency of languages versus C lang:
![cmp lang vs C](/ecoconception_logicielle/postpro_scripts/ecolang_compare_lang.png)

