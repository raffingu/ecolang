#!/bin/bash
DO_ALL=0 # only CB TP

CURRENT=`pwd`

set -eux ;

if [ "$DO_ALL" -eq "1" ] ; then
	echo "no-op"
fi


# CB TP:
cd $CURRENT/ecoconception_logicielle/
./setup-all.sh


# C
./cfiles/1DC
./cfiles/1DC_opt

# Fortran
./ffiles/1Dfortran 
./ffiles/1Dfortran_opt 

# Java
./javafiles/1Djava
./javafiles/1Djava_opt

# Julia
./jlfiles/1Djulia
./jlfiles/1Djulia_opt


# Tests:

# Python
#./pyfiles/1Dpython_numpy
./pyfiles/1Dpython_transonic
./pyfiles/1Dpython_numba

# Rust:
./rustfiles/1Drust
./rustfiles/1Drust_opt
./rustfiles/1Drust_opt3

# Go:
./gofiles/1Dgo
./gofiles/1Dgo_opt

echo "Test done"

