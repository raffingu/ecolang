# Use make to clean / build war file and create docker image:
make clean
make
# or make force-build (to clean cache)

# Run docker image:
docker images
docker run <imageId>

# Access to docker container:
docker ps
docker exec -it <containerId> bash


To access to X11:
    xhost +local:docker

