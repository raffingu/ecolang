\documentclass[a4paper,10pt]{article}

\usepackage{tp}
\usepackage{nicefrac}
\usepackage{multirow}
\usepackage{url}
\usepackage{listings}
\usepackage{color}
\usepackage{tabularx}
\usepackage{fancyvrb}
\usepackage{color}

\definecolor{olivegreen}{rgb}{0,0.6,0}

% Couleurs
\definecolor{vertfonce}{rgb}{0.5,0.8,0.3}
\definecolor{vert}{rgb}{0.5,0.9,0.5}
\definecolor{saumon}{rgb}{0.9,0.6,0.6}
\definecolor{bleu}{rgb}{0.2,0.2,0.7}
\definecolor{bleuClair}{rgb}{0.8,1,1}
\newcommand{\ro}[1]{\textcolor{red}{#1}}

\newcounter{c} 
\setcounter{c}{1}

\newcommand{\ques}[2]{
	\colorbox{#1}{
		\begin{minipage}{14cm}
			\textbf{Question \arabic{c}}\\
			#2
			\addtocounter{c}{1}
	\end{minipage}}
	\bigskip
}

\newcommand{\quesloc}[1]{\ques{bleuClair}{#1}}
\newcommand{\quesfront}[1]{\ques{vertfonce}{#1}}
\newcommand{\quesnode}[1]{\ques{saumon}{#1}}



\TDtitle{ENVOL 2023 - Impacts des algorithmes et des langages}
\TDnumber{0}
\TDdate{17/11/2023}

\TDauthor{ Laurent Bourg\`es (CNRS - OSUG), Cyrille Bonamy (CNRS - LEGI) - Laurent Lef\`evre (Inria Avalon) }
 

\setboolean{comment}{true}
\setboolean{correction}{false}

\definecolor{gris}{gray}{0.85}


\begin{document}

\lstset{emph={ssh},emphstyle=\textbf}

\TDmaketitle


\begin{comment}
Démarrer une machine dédiée sur la plate-forme Grid’5000 avant l'introduction.
\end{comment}

\section*{Introduction}
Cette séance de travaux pratiques va vous permettre d'évaluer l'efficacité énergétique des algorithmes, de plusieurs langages de programmation et vous montrera l'importance des optimisations et du dimensionnement (scalabilité) des algorithmes.\\

Le cas d'étude considéré est un code de calcul utilisé lors d'un cours de mécanique des fluides par le Maitre de Conférences Julien Chauchat. Pour les personnes intéressées, le cours associé à ce code est accessible via le fichier FormationVFMecaFlu.pdf.\\
Dans le cadre de ce TP, le même algorithme a été codé dans les langages Python, Julia, Fortran, C, Java, Go et Rust. Pour un même langage, plusieurs versions du code ont été implémentées pour évaluer certaines optimisations.\\
Dans un premier temps, nous allons exécuter les diverses versions du code, en faisant attention aux temps de réponse de chaque version. Puis nous vérifierons que le temps de calcul est bien relié à consommation électrique. \\ 
Nous allons donc travailler sur la plate-forme Grid’5000 pour mesurer la consommation électrique d’une machine de calcul pendant chaque expérience afin d'évaluer les 2 critères : temps de calcul et consommation énergétique. \\
Le but est de mettre en lumière certaines bonnes pratiques du développement logiciel.

\begin{comment}
Tous les fichiers sont disponibles dans un dépôt public (GPL2): \\
\url{https://gricad-gitlab.univ-grenoble-alpes.fr/ecoinfo/ecolang} \\

Le dossier \textbf{ecoconception\_logicielle} contient des scripts shells et les implémentations du code dans les sous-répertoires cfiles (C), ffiles (Fortran), gofiles (Go), javafiles (Java), jlFiles (Julia), pyfiles (Python) et rustFiles (Rust).
\end{comment}



\section{Accès machine Grid’5000 - cluster Lyon/nova}
Au préalable, vous avez obtenu un accès (login) sur la plateforme Grid’5000 par email et une clé SSH a été générée sur votre poste pour la renseigner dans votre compte Grid’5000. \\

Quelques liens utiles:
\begin{itemize}
	\item Monika / Lyon (réservation) : \url{https://intranet.grid5000.fr/oar/Lyon/monika.cgi}
	\item Grafana / Lyon (supervision): \url{https://api.grid5000.fr/stable/sites/lyon/metrics/dashboard/d/kwollect/kwollect-metrics}
\end{itemize} 

Les questions ont un code couleur qui précise \colorbox{vertfonce}{si vous travaillez sur la frontale de Grid'5000}, \\
\colorbox{saumon}{si vous travaillez sur votre nœud réservé} ou \colorbox{bleuClair}{si vous travaillez sur votre machine locale} .\\

\subsection{Accès à un noeud de calcul reservé}

\quesloc{Dans un terminal, connectez vous à une machine d'accès via la commande\\
\texttt{ssh <login>@access.grid5000.fr}\\
puis connectez vous à la frontale (ou \emph{frontend}) de Lyon via la commande\\
\texttt{ssh lyon}
}

Dans le cadre de ce TP, tout le cluster \texttt{Lyon - nova} a été reservé (20 noeuds). \\
Vous allez donc travailler au sein de ressources réservées appelées \emph{container job} et repéré par un \texttt{JOB\_ID} qui vous sera donné au début du TP.

\texttt{JOB\_ID: 1614676} \\

\quesfront{Réserver un nœud du cluster nova pour une durée de 3h avec l'option de monitoring haute fréquence (50Hz) via la commande\\
\texttt{oarsub -t deploy -l "host=1,walltime=03:00" -t monitor="wattmetre\_power\_watt" "sleep infinity" -t inner=1614676 -{}-project lab-2023-anf-envol } \\
}

L'interface OAR va attribuer un noeud ainsi qu'un nouveau job ID et afficher son identifiant: \\
\texttt{OAR\_JOB\_ID=1614829} \\

\quesfront{Récupérer le nœud nova réservé via la commande\\
oarstat -u}

\begin{bash}
Job id     Name           User           Submission Date     S Queue
---------- -------------- -------------- ------------------- - ----------
1614912                   lbourges       2023-11-16 09:19:35 R default   
\end{bash}

\quesfront{Récupérer le nom du noeud réservé via la commande\\
\texttt{export OAR\_JOB\_ID=1614912} \\
\texttt{oarstat -j \$OAR\_JOB\_ID -f | grep assigned\_hostnames} \\
}

L'interface répond : \\
\texttt{assigned\_hostnames = nova-XX.lyon.grid5000.fr } \\

\quesfront{Déployez l'image de l'OS préparée pour ce TP avec l'outil \texttt{kadeploy3} via la commande: \\
\texttt{kadeploy3 -m nova-XX.lyon.grid5000.fr -e ubuntu2004-tp2-cr17-ecolang-2 -u lbourges} \\
Le déploiement de l'image va prendre quelques minutes. \\
Lorsque le déploiement est terminé, vous pouvez vous connecter au nœud avec la commande: \\
\texttt{ssh root@<nova-XX} }

\quesnode{Sur le nœud de calcul, les fichiers du TP sont installés dans le répertoire \texttt{ecoconception\_logicielle} . \\
\texttt{cd ecoconception\_logicielle} \\
Lister les fichiers et dossiers avec la commande \\
\texttt{ls -l} }

\begin{bash}
bench-settings.sh  cpu_list_cpu_cores.sh  jlfiles          run-bench-MT.sh
cfiles             cpu_normal.sh          log_env.sh       run-bench.sh
clean-all.sh       cpu_show_freq.sh       matlab           run-gov-C_opt.sh
clean-bench.sh     env-all.sh             plot.sh          run-gov.sh
cpu_fixed.sh       env-bench.sh           postpro_scripts  run-hpl.sh
cpu_hpc.sh         ffiles                 pyfiles          rustfiles
cpu_hpc_turbo.sh   gofiles                results          setup-all.sh
cpu_ht_off.sh      hpl                    run-all.sh       setup-bench.sh
cpu_ht_on.sh       javafiles              run-bench-1T.sh
\end{bash}

\quesnode{Pour lister les langages installés et leurs versions, utiliser le script \texttt{env-all.sh} :\\
\texttt{./env-all.sh} \\
Quelle est la distribution linux et sa version ? \\
}

\begin{bash}
env-all: start ...
---------------------------------------
Environment on command 'cfiles/1DC' ...
CC version:
gcc (Ubuntu 9.4.0-1ubuntu1~20.04.2) 9.4.0
Copyright (C) 2019 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

CCLAGS : -Wall -O3
---------------------------------------
Environment on command 'jlfiles/1Djulia' ...
Julia version:
julia version 1.9.3
---------------------------------------
---------------------------------------
Environment on command 'pyfiles/1Dpython_numpy' ...
Python version:
Python 3.8.10
...
\end{bash}


\subsection{Collecte de la consommation électrique du noeud}

Un second terminal sur la frontale sera utilisé pour les transferts de fichiers et récupérer les mesures de consommation électrique de l'interface kwollect. \\

\quesloc{Dans un \textbf{second} terminal, connectez vous à une machine d'accès via la commande\\
\texttt{ssh <login>@access.grid5000.fr}\\
puis connectez vous à la frontale (ou \emph{frontend}) de Lyon via la commande\\
\texttt{ssh lyon}
}

Afin de faciliter la récupération des données de consommation depuis l'API Kwollect et leur visualisation, nous avons préparé quelques scripts pour ce TP. \\

\quesfront{La première fois, veuillez récupérer et extraire l'archive avec les scripts avec les commandes suivantes: \\
\texttt{wget }\url{http://public.lyon.grid5000.fr/\textasciitilde vostapenco/tp-consumption-cr17/scripts\_front.tar.gz} \\
\texttt{tar -xvf scripts\_front.tar.gz} \\
}

\quesfront{
Utiliser le script \texttt{get\_consumption\_kwollect.py} pour récupérer les données de consommation du nœud entre deux dates au format CSV :\\
\texttt{python3 get\_consumption\_kwollect.py -{}-site lyon -{}-host nova-9 -{}-start 2023-11-16T09:45:00 -{}-end 2023-11-16T09:55:00 -{}-csv kwollect\_TEST-CASE.csv} \\
Au début du TP, vous pouvez récupérer la consommation pendant la phase (START) entre l'heure de début du déploiement (kadeploy) et maintenant.
}

\quesfront{
Utiliser le script \texttt{plot\_consumption\_kwollect.py} pour créer un graphique de consommation à partir du fichier CSV : \\
\texttt{python3 plot\_consumption\_kwollect.py -{}-csv kwollect\_TEST-CASE.csv -{}-plot kwollect\_TEST-CASE.jpg} \\
}

\quesfront{
Utiliser le script \texttt{calculate\_energy\_kwollect.py} pour calculer l'énergie consommée par le nœud à partir du fichier CSV : \\
\texttt{python3 calculate\_energy\_kwollect.py -{}-csv kwollect\_TEST-CASE.csv} \\
}

\begin{bash}
Wattmeter energy (Joules): 40170.87093419999
Wattmeter energy (Wh): 11.158575259499997
\end{bash}

\quesloc{
Récupérer le graphique de consommation dans un \textbf{troisième} terminal : \\
\texttt{scp <login>@access.grid5000.fr:lyon/kwollect\_TEST-CASE.jpg . } \\
}

\begin{comment}
Afin de bien suivre les opérations et la reproductibilité, il est recommandé d'utiliser un journal de bord sous la forme d'un fichier texte pour garder l'historique des expériences avec un identifiant [TEST-CASE-1] les dates de début et de fin ainsi que les conditions de l'expérience. \\ 
Dans la suite du TP, vous aurez l'occasion d'utiliser des scripts pour lancer les expériences qui tracent tous les résultats afin de garder l'historique complet des expériences. \\
Il est recommandé d'utiliser l'identifiant d'expérience dans les noms de fichier de sorties (graphique, données ...) pour plus de clarté. \\

Veuillez bien garder les terminaux ouverts et utiliser l'historique pour répéter ces enchaînements de commande afin de récupérer la consommation électrique, faire le graphique et calculer l’énergie consommée par l'expérience.
\end{comment}



\section{Prise en main du code scientifique (Python)}

Pour consulter les codes utilisés pendant ce TP, en local ou via l'interface Gitlab (web), il suffit de consulter le dépôt public \texttt{ecolang} : \\
\url{https://gricad-gitlab.univ-grenoble-alpes.fr/ecoinfo/ecolang} \\

Nous allons étudier la version originale du code Python en termes de fonctionnalité, quelques implémentations différentes (optimisées) et la performance des algorithmes. \\

Dans cette section, les fichiers sont dans le répertoire: \\
\texttt{ecoconception\_logicielle/pyfiles/deprecated/} \\

\quesnode{Sur le nœud de calcul, aller dans le répertoire : \\
\texttt{cd ecoconception\_logicielle} \\
\texttt{ls -l} \\
}

\begin{bash}
-rw-r--r-- 1 1000 1000 4580 Nov 14 14:31 main1D.py
-rw-r--r-- 1 1000 1000 5278 Nov 14 14:31 main1D_cb.py
-rw-r--r-- 1 1000 1000 4330 Nov 14 14:27 main1D_cbv1.py
-rw-r--r-- 1 1000 1000 5368 Nov 14 14:28 main1D_cbv4_plot.py
\end{bash}

Voici la fiche de résultats pour cette partie:
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
Langage     &   programme         &   Temps (s) & Taille (M) \\ \hline
Python      &   main1D.py   &   & 2048 \\ \hline
Python      &   main1D\_cb.py   &   & 2048 \\ \hline
Python      &   main1D.py   &   & 4096 \\ \hline
Python      &   main1D\_cb.py   &   & 4096 \\ \hline
Python      &   main1D\_cbv1.py   &   & \\ \hline
Python      &   main1D\_cbv4\_plot.py   &   & \\ \hline
\end{tabular}
\end{center}



\subsection{Python : main1D.py - version originale}

Implementation: \texttt{main1D.py} \\
Algorithme: 	matriciel (2048 pt) \\

\quesnode{Exécuter la version originale de l'algorithme : :\\
\texttt{python3 main1D.py} \\
}

\begin{bash}
case :  diffusion
M  :  2048
dx :  0.0004885197850512946
dt :  0.1
eps:  1e-09
Matrice M x M:  32768.0  kb
k= 0 : RMS U = 0.045408200843564415
k= 1 : RMS U = 0.022800814991688578
k= 2 : RMS U = 0.011474216548153275
k= 3 : RMS U = 0.00577473863179966
k= 4 : RMS U = 0.0029063177386033294
k= 5 : RMS U = 0.001462695455104649
k= 6 : RMS U = 0.0007361473171634006
k= 7 : RMS U = 0.00037048920247915833
k= 8 : RMS U = 0.00018646029944925454
k= 9 : RMS U = 9.384198802950539e-05
k= 10 : RMS U = 4.722892067053654e-05
k= 11 : RMS U = 2.376943408366081e-05
k= 12 : RMS U = 1.1962712477294349e-05
k= 13 : RMS U = 6.020609889342228e-06
k= 14 : RMS U = 3.0300605246560863e-06
k= 15 : RMS U = 1.524973002460496e-06
k= 16 : RMS U = 7.674903935427645e-07
k= 17 : RMS U = 3.862634675551273e-07
k= 18 : RMS U = 1.94399290516959e-07
k= 19 : RMS U = 9.78375961565514e-08
k= 20 : RMS U = 4.923981920865934e-08
k= 21 : RMS U = 2.4781404322828395e-08
k= 22 : RMS U = 1.2472116265092462e-08
k= 23 : RMS U = 6.276992430093799e-09
k= 24 : RMS U = 3.159007987581329e-09
k= 25 : RMS U = 1.589892257133484e-09
The simulation has converged in  26  iterations
Time in seconds =  5.642541324999911
result samples:
u[ 0 ] =  3.660975555710436e-12
u[ 204 ] =  0.044863155641457694
u[ 408 ] =  0.07979458714416685
u[ 612 ] =  0.1047942945426996
u[ 816 ] =  0.11986227786163131
u[ 1020 ] =  0.12499853711690102
u[ 1224 ] =  0.12020307231402412
u[ 1428 ] =  0.10547588344773287
u[ 1632 ] =  0.08081697050254508
u[ 1836 ] =  0.046226333454115504
u[ 2040 ] =  0.0017039722716841726
\end{bash}

Le programme donne les conditions initiales, le temps d’exécution et quelques résultats.

\begin{question}
Que fait le programme ? \\
Quels sont les résultats ? \\
Combien de répétitions ? d'itérations ? \\
Quelle est la durée donnée par le programme ?
\end{question}


\begin{question}
Exécutez à nouveau le script en utilisant la commande time afin de récupérer le temps CPU. \\
Quelle est l'unité de mesure ? Est-ce cohérent avec la mesure du programme [Time in seconds] ?
\end{question}

Lancer vmstat ou htop pour observer la charge de la machine:
\begin{bash}
vmstat 1
htop
\end{bash}

\begin{question}
Quelle est la consommation CPU et de mémoire du programme ?  \\
Est-ce que les programmes utilisent le multi-threading (1T ou plus) ?
\end{question}

\quesnode{Regarder le code original (ou en local):\\
\texttt{vi main1D.py} \\
}

\begin{question}
Quelle est la taille du problème ? \\
Quel algorithme est utilisé ? \\
Quelle est la précision des résultats ?
\end{question}


\subsection{Python : main1D\_cb.py - TDMA}

Implementation: \texttt{main1D\_cb.py} \\
Algorithme: 	numpy TDMA (2048 pt) \\

\quesnode{Exécuter la version modifiée par Cyrille Bonamy de l'algorithme : :\\
\texttt{python3 main1D\_cb.py} \\
}

\quesnode{Quelles sont les différences à l'aide de la commande :\\
\texttt{diff main1D.py main1D\_cb.py} \\
}

\begin{question}
Quelle est la taille du problème ? \\
Quel algorithme est utilisé ? \\
Est-ce que les résultats sont identiques ? \\
Combien de répétitions ? d'itérations ? \\
Quelle est la durée donnée par le programme ?
Quel est le gain de performance et l'impact sur la consommation de mémoire ?
\end{question}


\subsection{Python : scalabilité VO vs TDMA}

\quesnode{Modifier les programmes pour changer M = 4096 :\\
\texttt{vi main1D.py} \\
\texttt{vi main1D\_cb.py} \\
Puis relancer les codes: \\
\texttt{python3 main1D.py} \\
\texttt{python3 main1D\_cb.py} \\
}

\begin{question}
Quel est l'impact sur les temps de réponse ? \\
Etablir le facteur de scalabilité (2048 vs 4096) pour les 2 versions ? \\
\end{question}

\begin{comment}
O($N^2$) vs O(N): TDMA bien adapté à ce problème précis => les temps de réponses sont excellents ! \\
\textbf{La taille du problème de 4 million de points pour la suite : c'est l'effet 'Rebond' !} \\
\end{comment}

\subsection{Python : main1D\_cbv1.py - référence}

Implementation: \texttt{main1D\_cbv1.py} \\
Algorithme: 	numpy TDMA (4M pt) \\

\quesnode{Exécuter la commande :\\
\texttt{python3 main1D\_cbv1.py} \\
}

\begin{question}
Quelle est la taille du problème ? \\
Quelle est la précision des résultats ?
Quel est l'impact sur les temps de réponse et de mémoire du programme ? \\
\end{question}

\begin{comment}
Numpy est lent, pourquoi ? \\
itérations directes a[i] dans solver => pas de fonction C optimisée => interpreteur python \\
=> Idée: comparer les accélérateurs python ensuite et d'autres langages HPC ou plus généralistes \\
\end{comment}


\subsection{Python : main1D\_cbv4\_plot.py - numba}

Cette version utilise la librairie numba pour compiler la fonction TDMASolver (JIT):

\quesnode{Quelles sont les différences à l'aide de la commande :\\
\texttt{diff main1D\_cbv1.py main1D\_cbv4\_plot.py} \\
}

\quesnode{Exécuter la commande :\\
\texttt{python3 main1D\_cbv4\_plot.py} \\
}

\begin{question}
Quel est l'impact sur les temps de réponse ? \\
Quel est le gain de performance ?
\end{question}

\begin{comment}
@jit est magique !
Il est possible d'éxécuter le code python numpy (pur) beaucoup plus vite à l'aide de librairies numba, transonic ... qui compilent le code critique (@jit) à la volée pour optimiser les performances. Jusqu'à quel point est-ce efficace en comparaison avec d'autres langages ?
\end{comment}

Enfin, vous pouvez regarder le monitoring Grafana pour observer la consommation eléctrique depuis le début de cette section à \url{https://api.grid5000.fr/stable/sites/lyon/metrics/dashboard/d/kwollect/kwollect-metrics}.

\begin{question}
Comment interpréter la consommation du noeud ?
\end{question}


\subsection{Python : visualisation (démo)}

Les programmes \texttt{main1D.py} et \texttt{main1D\_cbv4\_plot.py} affichent les résultats en fin de calcul à l'aide de la librairie connue matplotlib. \\

(en raison de l'utilisation des serveurs (X non disponible ?), une démo va être présentée)

\quesloc{Exécuter la commande :\\
\texttt{python3 main1D.py} \\
Puis : \\
\texttt{python3 main1D\_cbv4\_plot.py} \\
}

\begin{question}
Quel est l'impact de la visualisation sur la machine ? (top) \\
\end{question}

\begin{comment}
4s de calcul mais plot très long à traiter car 4M pt (déjà en local mais pire à distance via ssh -X !!) \\

Il faut utiliser poste utilisateur (plus efficace?) pour la visualisation et des algorithmes adaptés (GPU, heat-map, spatial indexes...) \\
C'est un métier et une expertise en soi !
\end{comment}

\begin{comment}
Attention au coût des traces (printf, stdout, log files) qui peut être non négligeable :
\begin{itemize}
\item gestion des niveaux de verbosité
\item volume des traces
\item synchronisation pénalisante en multi-thread)
\end{itemize}
\end{comment}




\section{Etude performance \& consommation énergie sur algorithme TDMA}

Dans cette section, nous allons utiliser les scripts shell pour s'assurer d'un protocole expérimentale reproductible et détaillé:
10 répétitions du programme pour les statistiques, les scripts shells pour compiler, lancer les expériences, stocker les résultats ... \\

Le principe des scripts est simple:
\begin{itemize}
\item	\texttt{cpu\_...sh} : scripts de gestion du CPU (governor, freq, hyper-threading)
\item	\texttt{setup-bench.sh} : script de compilation du langage donné
\item	\texttt{run-bench-1T.sh} : test du langage donné (single-thread)
\item	\texttt{run-bench-MT.sh} : test du langage donné (multi-thread), use \texttt{CPU\_CORES} variable
\end{itemize}

Les implémentations dans les différents langages sont stockées dans les sous-répertoires \texttt{cfiles} (C), \texttt{ffiles} (Fortran), \texttt{gofiles} (Go), \texttt{javafiles} (Java), \texttt{jlFiles} (Julia), \texttt{pyfiles} (Python) et \texttt{rustFiles} (Rust). \\


Voici la fiche de résultats pour cette partie:
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
Time &   Langage     &   programme         & Threads    &   Temps (s)   &   Energie (W.h) \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
    &    &    &    &    & \\ \hline
\end{tabular}
\end{center}

Pour commençer, nous allons exécuter le code Python numpy de référence avec le protocole de test:

\quesnode{Lancer l'expérience à l'aide de la commande :\\
\texttt{cd ecoconception\_logicielle} \\
\texttt{./run-bench-1T.sh pyfiles/1Dpython\_numpy} \\
Et regarder le code :\\
\texttt{vi pyfiles/1Dpython\_numpy} \\
}

\begin{question}
Quels changements sont apportés par le protocole ? \\
Les temps de réponses sont-ils cohérents avec les précédentes expériences ?
\end{question}

A l'aide des scripts, il est donc possible de lancer des expériences reproductibles dans les langages supportés (voir sous-dossiers).


\subsection{Tests single-thread}

Dans cette section, quelques langages vont être testés: python, C, julia ... et vous utiliserez les wattemètres de la plate-forme Grid'5000 (cf partie 1) pour récupérer le profil énergétique (consommation électrique pendant l'expérience), le graphique correspondant et la quantité d'enèrgie consommée (W.h). \\

\quesnode{Lancer l'expérience à l'aide de la commande :\\
\texttt{cd ecoconception\_logicielle} \\
\texttt{./run-bench-1T.sh cfiles/1DC} \\
\texttt{./run-bench-1T.sh jlfiles/1Djulia} \\
Et regarder le code :\\
\texttt{vi cfiles/main1D\_cb.c} \\
\texttt{vi jlfiles/main1D\_cbv3.jl} \\
}

\begin{question}
Est-ce que les programmes donnent les mêmes résultats numériques ? \\
Est-ce plus performant que les programmes Python ?
Faites une mesure de la consommation électrique des diverses versions des expériences Python, C et Julia.\\
Remplir la Fiche de résultats (temps CPU et consommation) \\
Quelles sont les incertitudes sur l'estimation de l'énergie consommée ?
Quels sont les différences en termes de performance et de consommation ?  \\
Observez les profils de consommation, qu'en pensez-vous ? \\
Quel est la consommation à vide (idle) ? \\
Quel est l'algorithme le plus efficace énergétiquement parlant ?
\end{question}


\subsection{Tests MT (hyper-threading)}

Dans cette section, les tests seront réalisés en utilisant plusieurs processus en parallèle pour reproduire un cas idéal de parallélisation idéale (zero over-head) et mesurer des différences importantes sur la consommation (charge de 50\% ou 100\%) \\

\quesnode{Lancer l'expérience à l'aide de la commande :\\
\texttt{cd ecoconception\_logicielle} \\
\texttt{./run-bench-MT.sh cfiles/1DC} \\
}

\begin{question}
Combien de threads / coeurs CPU sont utilisés ? \\
Faites une mesure de la consommation électrique de cette expériences C (MT).\\
Remplir la Fiche de résultats (temps CPU et consommation) \\
Quels sont les différences en termes de performance et de consommation ? \\
Sachant que ce protocole (MT) effectue 10 itération par thread, le travail réalisé est multiplié par le nombre de processus (= CPU\_CORES). \\
Quel est l'éfficacité du programme 1T et MT ?
\end{question}

\quesnode{Lancer l'expérience sans CPU Hyper-threading à l'aide de la commande :\\
\texttt{cd ecoconception\_logicielle} \\
\texttt{./cpu\_ht\_off.sh } \\
\texttt{./run-bench-MT.sh cfiles/1DC} \\
}

\begin{question}
Combien de threads / coeurs CPU sont utilisés ? \\
Faites une mesure de la consommation électrique de cette expériences C (MT).\\
Remplir la Fiche de résultats (temps CPU et consommation) \\
Quels sont les différences en termes de performance et de consommation ?
\end{question}

Le script \texttt{./run-bench-MT.sh} utilise la variable \texttt{CPU\_CORES} pour définir le nombre de processus à lancer. \\

\quesnode{Lancer l'expérience avec 8 coeurs utilisés à l'aide de la commande :\\
\texttt{cd ecoconception\_logicielle} \\
\texttt{CPU\_CORES=8 ./run-bench-MT.sh cfiles/1DC} \\
}

\begin{question}
Faites une mesure de la consommation électrique de cette expérience.\\
Remplir la Fiche de résultats (temps CPU et consommation) \\
Quels sont les différences en termes de performance et de consommation ? \\
Quel est l'éfficacité du programme MT dans ce cas ? \\
Pouvez-vous tracer le profil énergétique en fonction de la charge (1T, 8T, 16T) ?
\end{question}



\subsection{Tests MT - Python}

Dans cette section, des expériences seront réalisées en utilisant les différentes version du code Python (python numpy, numba ou transonic) pour évaluer les gains des accélarateurs transonic et numba. \\

\quesnode{Lancer l'expérience à l'aide de la commande :\\
\texttt{cd ecoconception\_logicielle} \\
\texttt{./run-bench-MT.sh pyfiles/1Dpython\_numba} \\
}

\begin{question}
Faites une mesure de la consommation électrique de cette expérience.\\
Remplir la Fiche de résultats (temps CPU et consommation) \\
Quels sont les différences en termes de performance et de consommation ? \\
Comparer avec l'experience C (MT)
\end{question}


\quesnode{Lancer l'expérience à l'aide de la commande :\\
\texttt{cd ecoconception\_logicielle} \\
\texttt{./run-bench-MT.sh pyfiles/1Dpython\_transonic} \\
}

\begin{question}
Faites une mesure de la consommation électrique de cette expérience.\\
Remplir la Fiche de résultats (temps CPU et consommation) \\
Quels sont les différences en termes de performance et de consommation ? \\
Comparer avec l'experience C et numba (MT)
\end{question}


Enfin si vous avez le temps (numpy est trés lent):

\quesnode{Lancer l'expérience à l'aide de la commande :\\
\texttt{cd ecoconception\_logicielle} \\
\texttt{./run-bench-MT.sh pyfiles/1Dpython\_numpy} \\
}

\begin{question}
Combien d'itérations sont réalisées ?
Faites une mesure de la consommation électrique de cette expérience.\\
Remplir la Fiche de résultats (temps CPU et consommation) \\
Quels sont les différences en termes de performance et de consommation ? \\
\end{question}

\begin{question}
Quels sont vos conclusions sur la comparaison des versions Python ?
\end{question}


\subsection{Tests MT - Autres langages}

Dans cette section, des expériences seront réalisées en utilisant les autres langages disponibles (Julia, Java, Fortran...). \\

\quesnode{Lancer les expériences à l'aide des commandes :\\
\texttt{cd ecoconception\_logicielle} \\
\texttt{./run-bench-MT.sh jlfiles/1Djulia} \\
\texttt{./run-bench-MT.sh jlfiles/1Djava} \\
\texttt{./run-bench-MT.sh jlfiles/1Dfortran} \\
...
}

\begin{question}
Faites une mesure de la consommation électrique de ces expériences.\\
Remplir la Fiche de résultats (temps CPU et consommation) \\
Quels sont les différences en termes de performance et de consommation ? \\
Comparer les portages ? sont-ils équivalents ?
\end{question}


\subsection{Conclusion sur les langages}

\begin{question}
Quel bilan tirer de cette étude sur les différents langages en terme de performance (CPU) et de consommation ? \\
\end{question}


\subsection{Etude performance \& consommation énergie sur algorithme TDMA optimisé (Bonus)}

Comparer les versions de référence et optimisée (\texttt{\_opt}) pour quelques langages: python transonic, C et julia ...

\begin{question}
Quel est l'impact des optimisations ? quel ordre de grandeur ?
Faites une mesure de la consommation électrique de ces expériences (MT).\\
Remplir la Fiche de résultats (temps CPU et consommation) \\
Quels sont les différences en termes de performance et de consommation par rapport aux codes de référence ? \\
\end{question}



\section{Impact de la volumétrie \& Scalabilité du programme }

Dans cette section, nous allons faire varier la taille du problème (M) pour évaluer la loi de montée à l'échelle (scalabilité) d'une implémentation de l'algorithme TDMA.

\quesnode{Modifier le programme \texttt{cfiles/main1D\_cb.c} pour changer M aux valeurs suivantes: 1000 * 1000, 100 000 puis 10 000 :\\
\texttt{vi cfiles/main1D\_cb.c} \\
Lancer l'expérience à l'aide de la commande :\\
\texttt{./run-bench-MT.sh cfiles/1DC} \\
}

\begin{question}
Lancer le programme pour le temps CPU pour M = 1000 * 1000, 100 000, 10 000, 1000. \\
Quel est l'impact sur les résultats ?
\end{question}

\begin{question}
Etablir la loi de scalabilité en fonction de la taille du problème (M) : Time vs M \\
Quel est le coût CPU (donc la consommation) pour doubler la taille du problème ? pour multiplier par 10 ?
Extrapolation: Quel temps CPU faudrait-il pour $10^9$ points, $10^12$ points (très grande échelle) ?
\end{question}

\begin{question}
Questions subsidiaires: 
quelle est la précision suffisante pour cette simulation ? \\
Quelle est la meilleure optimisation entre le choix du langage et adapter les paramètres de la simulation ? \\
Dans quel cas, faut-il priviligier l'une ou l'autre des approches ?
\end{question}


\begin{comment}
Dans cette algorithme itératif (26 itérations), la convergence est déterminée par delta < eps = 1e-9. \\
Pour avoir un résultat rapide mais approximatif, il est possible de changer le paramètre eps à 1e-6 voire moins pour calculer moins d'itérations et réduire les impacts.
\end{comment}


\section{Impact de Docker et Intégration continue (EXTRA) }

L'utilisation des technologies de container (Docker) et de l'intégration continue sont très 'à la mode' ...

Tous les codes des TPs EcoConception logicielle sont gérés dans un dépot gitlab sur la forge GRICAD (U.G.A). \\
Ces codes ont été préparés pour fonctionner sur un serveur Ubuntu 20.04 et Docker a été utilisé pour valider le bon fonctionnement (tests d'intégration) sur ubuntu 18, 20 et 22 et debian 10 et 12. \\

\subsubsection{Impact de l'utilisation de Docker}

Docker et la procédure d'installation (image Ubuntu + paquets debian + dépendances julia, python) nécessite le réseau : environ 2GB (download)...

\begin{bash}
docker images

REPOSITORY                                                  TAG                     IMAGE ID       CREATED         SIZE
ecoinfo_anf                                                 0.2                     173821527559   20 hours ago    2.02GB
ubuntu                                                      18.04                   3556258649b2   23 months ago   64.2MB
\end{bash}

Lancer la construction de l'image docker : \\
\begin{bash}
cd setup/docker_ubuntu20
make
\end{bash}

\begin{question}
Quelles sont les étapes de construction de l'image (DockerFile) ? \\
Quels sont les impacts (réseau, temps CPU, consommation) ?
\end{question}


\subsubsection{Impact de l'intégration continue}

Gitlab propose une intégration continue basée sur Docker.

Voir le fichier .gitlab-ci.yml:
\begin{bash}
image: ubuntu:18.04

job_test:
    before_script:
        - apt-get update
        - apt-get upgrade -y 
        - apt-get install -y locales
        - localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
        - export LANG='en_US.UTF-8'
        - export LANGUAGE='en_US:en'
        - export LC_ALL='en_US.UTF-8'
        - cd $CI_PROJECT_DIR
        - cd ateliers/dev
        - bash setup.sh

    script: 
        - cd $CI_PROJECT_DIR
        - cd ateliers/dev
        - bash run_tests.sh > test.log

    when: manual
#    when: delayed
#      start_in: 30 minutes

    artifacts:
        paths:
          - ateliers/dev/test.log
        when: always
\end{bash}

Voir les jobs sur gitlab:
\url{https://gricad-gitlab.univ-grenoble-alpes.fr/ecoinfo/anf2021/-/jobs/}

\begin{question}
Quelle est la fréquence de lancement du job ? (18 minutes 25 seconds) \\
Quelle est la taille de l'artefact (paquet produit) ? (17 Mb) \\
Quelle volumétrie représente 1000 téléchargement ? 10 utilisateurs avec des mises à jour tous les mois sur 1 an ? \\
Quel impact (réseau, CPU) a l'intégration continue si elle était lancée chaque jour ? à chaque commit (10 par jour) ?
\end{question}


\end{document}
