#!/bin/bash

cd ecoconception_logicielle
./clean-all.sh
cd ..

tar cvfz tarball.tar.gz --exclude="package.sh" *.sh --exclude="tarball.tar.gz" --exclude="ecoconception_logicielle/results/*" --exclude="ecoconception_logicielle/postpro_scripts/*" ecoconception_logicielle 

