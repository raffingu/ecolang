
## Tri Diagonal Matrix Algorithm(a.k.a Thomas algorithm) solver
function TDMAsolver(a, b, c, d)
    """
    TDMA solver, a b c d can be NumPy array type or Python list type.
    refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    """
    (nf,) = size(b)  # number of equations
    bc = copy(b)     # copy the array
    dc = copy(d)     # copy the array

    for it in 2:nf
        mc = a[it - 1] / bc[it - 1]
        bc[it] = bc[it] - mc *  c[it - 1]
        dc[it] = dc[it] - mc * dc[it - 1]
    end
    xc = bc
    xc[nf] = dc[nf] / bc[nf]

    for il in nf-1:-1:1
        xc[il] = (dc[il] - c[il] * xc[il + 1]) / bc[il]
    end
    return xc
end

function RMS_E(X1, X2)
    (sizeX1,) = size(X1)
    return sqrt(sum((X1 - X2).^2) / sizeX1)
end

function solve(M, dbg)
    start = time_ns()
    #
    #
    #     INPUT PARAMETERS
    #
    # Domain length
    Lx = 1.0

    # mesh cells in x-direction
    # M = 2048 * 2048

    # Grid sizes
    dx = Lx / (M - 1)

    # Pressure gradient
    dpdx = 1.0

    # Reynolds number: Re = U L / nu
    Re = 1.0

    # Diffusivity
    Gamma = 1.0 / Re

    # Velocity
    c0 = 0.1

    # Time step:
    dt = 0.1

    # Configuration
    case = "diffusion"
    #case = "convection"

    #
    #     INITIALISATION
    #


    if (case == "diffusion")
        # Pressure gradient
        dpdx = 1.0
        # Velocity
        c0 = 0.0
        # Diffusivity
        Gamma = 1.0 / Re
        # Time step:
        dt = 0.1
        # Maximum number of iterations
        maxIter = 1000
    else
        println("Invalid case = ", case)
        return # failure
    end

    # Precision
    eps = 1e-9

    #
    # Print initial conditions:
    if (dbg == 1)
        println("case : ",case)
        println("M  : ", M)
        println("dx : ", dx)
        println("dt : ", dt)
        println("eps: ", eps)
    end


    # boundary conditions
    BCNorth = 0
    BCSouth = 0

    # Initialisation of variables
    Aua = zeros(M - 1)
    Aub = zeros(M)
    Auc = zeros(M - 1)
    Aud = zeros(M)

    uold = zeros(M)
    u    = []

    #
    # Convection fluxes
    #
    Fe = c0
    Fw = c0
    #
    # Diffusion fluxes
    #
    De = Gamma / dx
    Dw = Gamma / dx
    #
    # Compute system coefficients
    #
    aE = De + max(-Fe, 0)
    aW = Dw + max( Fw, 0)
    aP = aE + aW + Fe - Fw
    #
    # Assemble matrix coefficients
    #
    Aua[1 : M - 1] .= -aW
    Aub[2 : M - 1] .= dx / dt + aP
    Auc[1 : M - 1] .= -aE

    # Min step check:
    last_rms = 1.0
    min_step = eps / 10.0

    # First iteration
    k = 0
    while k < maxIter
        # Predictor step
        #
        # Convection fluxes
        #
        Aud = dx / dt * uold .+ dpdx * dx

        # Boundary conditions
        # North boundary
        if (BCNorth == 0) # Dirichlet
            Aub[M] = 1.0
            Aud[M] = 0.0
        elseif (BCNorth == 1) # Neumann
            Aua[M - 1] = -1.0
            Aub[M] = 1.0
            Aud[M] = 0.0
        end
        # South boundary
        if (BCSouth == 0) # Dirichlet
            Aub[1] = 1.0
            Aud[1] = 0.0
        elseif (BCSouth == 1) # Neumann
            Auc[1] = -1.0
            Aub[1] = 1.0
            Aud[1] = 0.0
        end

        # Solve algebraic system
        u = TDMAsolver(Aua, Aub, Auc, Aud)

        # Compute rms error at the end of each time step
        rms_u = RMS_E(u, uold)

        if (rms_u < eps)
            if (dbg == 1)
                println("The simulation has converged in ", k, " iterations")
            end
            break
        elseif (abs(last_rms - rms_u) < min_step)
            if (dbg == 1)
                println("The simulation convergence is limited to ", rms_u, " in ", k, " iterations")
            end
            break
        else
            if (dbg == 1)
                println("k=", k, ": RMS U =", rms_u)
            end
        end
        uold = u
        k = k + 1
        last_rms = rms_u
    end

    endtime = time_ns()
    if (dbg == 1)
        println("|Bench| Time (s): ", (endtime - start) / 1.0e9)

        println("result samples:")
        for idx in 0:trunc(Int32, M/10):M
            println("u[", idx, "] = ", u[idx + 1])
        end
    end
end

function main()
    WI = 100
    WM = 64 * 1024
    BI = 10
    BM = 1 * 2048 * 2048

    println("|Bench| WARMUP ", WI, " iterations [len = ", WM,"]---")
    tv = read(`date +%s`, String)
    println("|Bench| WARMUP Start: ", tv)

    for it in 1:WI
        solve(WM, 0)
    end

    tv = read(`date +%s`, String)
    println("|Bench| WARMUP Stop: ", tv)

    sleep(3)

    println("|Bench| BENCHMARK ", BI, " iterations [", BM, "]---")
    tv = read(`date +%s`, String)
    println("|Bench| BENCHMARK Start: ", tv)

    for it in 1:BI
        solve(BM, 1) # show logs
    end

    tv = read(`date +%s`, String)
    println("|Bench| BENCHMARK Stop: ", tv)
end

main()

