#!/bin/bash
# Needs sudo

# Use US Locale:
export LANG=C

# always report cpu 0 (always online, but missing flag):
echo "0"

for i in $(ls /sys/devices/system/cpu/cpu*/online); do 
    ENABLE=`cat ${i}`
    if [ "${ENABLE}" -eq "1" ]; then
        echo "${i}" | awk -F'/' '{gsub("cpu","",$6);print $6}'
    fi
done

