#!/bin/bash

# Use US Locale:
export LANG=C

HOST_ENV=results/host_env.txt

echo "HOST Environment:" > $HOST_ENV

echo "---" >> $HOST_ENV
echo "HOST:" >> $HOST_ENV
hostname >> $HOST_ENV

echo "---" >> $HOST_ENV
echo "OS:" >> $HOST_ENV
lsb_release -a >> $HOST_ENV

echo "---" >> $HOST_ENV
echo "CPU:" >> $HOST_ENV
lscpu >> $HOST_ENV

# log CPU 0 freqs:
echo "---" >> $HOST_ENV
cpupower frequency-info  >> $HOST_ENV

echo "---" >> $HOST_ENV
echo "GCC:" >> $HOST_ENV
gcc --version >> $HOST_ENV

echo "---" >> $HOST_ENV
echo "LIBC:" >> $HOST_ENV
ldd --version >> $HOST_ENV

echo "---" >> $HOST_ENV

