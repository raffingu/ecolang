""""
EcoInfo - Time & energy efficiency of programming languages in HPC
Copyright (C) 2020, CNRS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import math
import time

from transonic import jit


## Tri Diagonal Matrix Algorithm(a.k.a Thomas algorithm) solver
@jit
def TDMAsolver(a, b, c, d):
    """
    TDMA solver, a b c d can be NumPy array type or Python list type.
    refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    """
    nf = len(b)    # number of equations
    bc = b.copy()  # copy the array
    dc = d.copy()  # copy the array

    for it in range(1, nf):
        mc = a[it - 1] / bc[it - 1]
        bc[it] = bc[it] - mc *  c[it - 1]
        dc[it] = dc[it] - mc * dc[it - 1]

    xc = bc
    xc[-1] = dc[-1] / bc[-1]

    for il in range(nf - 2, -1, -1):
        xc[il] = (dc[il] - c[il] * xc[il + 1]) / bc[il]

    return xc

@jit
def RMS_E(X1, X2):
    rms = math.sqrt(((X1 - X2) ** 2).sum() / len(X1))
    return rms


def solve(M, dbg):
    start = (time.time())

    #
    #
    #     INPUT PARAMETERS
    #
    # Domain length
    Lx = 1.0

    # Grid sizes
    dx = Lx / (M - 1)

    # Pressure gradient
    dpdx = 1.0

    # Reynolds number: Re = U L / nu
    Re = 1.0

    # Diffusivity
    Gamma = 1.0 / Re

    # Velocity
    c0 = 0.1

    # Time step:
    dt = 0.1

    # Configuration
    case = "diffusion"
    #case = "convection"

    #
    #     INITIALISATION
    #


    if (case == "diffusion"):
        # Pressure gradient
        dpdx = 1.0
        # Velocity
        c0 = 0.0
        # Diffusivity
        Gamma = 1.0 / Re
        # Time step:
        dt = 0.1
        # Maximum number of iterations
        maxIter = 1000
    else:
        print("Invalid case = ", case)
        return 1 # failure


    # Precision
    eps = 1e-9

    #
    # Print initial conditions
    if (dbg == 1):
        print("case : ",case)
        print("M  : ", M)
        print("dx : ", dx)
        print("dt : ", dt)
        print("eps: ", eps)


    # boundary conditions
    BCNorth = 0
    BCSouth = 0

    # Initialisation of variables
    Aua = np.zeros([M - 1], dtype=float)
    Aub = np.zeros([M], dtype=float)
    Auc = np.zeros([M - 1], dtype=float)
    Aud = np.zeros([M], dtype=float)

    uold = np.zeros([M], dtype=float)
    u    = np.zeros([0], dtype=float)

    #
    # Convection fluxes
    #
    Fe = c0
    Fw = c0
    #
    # Diffusion fluxes
    #
    De = Gamma / dx
    Dw = Gamma / dx
    #
    # Compute system coefficients
    #
    aE = De + max(-Fe, 0)
    aW = Dw + max( Fw, 0)
    aP = aE + aW + Fe - Fw
    #
    # Assemble matrix coefficients
    #
    Aua[:]         = -aW
    Aub[1 : M - 1] = dx / dt + aP
    Auc[:]         = -aE

    # Min step check:
    last_rms = 1.0
    min_step = eps / 10.0

    # First iteration
    k = int(0)
    while k < maxIter:
        # Predictor step
        #
        # Convection fluxes
        #
        Aud[:] = dx / dt * uold[:] + dpdx * dx

        # Boundary conditions
        # North boundary
        if (BCNorth == 0):  # Dirichlet
            Aub[M - 1] = 1.0
            Aud[M - 1] = 0.0
        elif (BCNorth == 1):  # Neumann
            Aua[M - 2] = -1.0
            Aub[M - 1] = 1.0
            Aud[M - 1] = 0.0
        # South boundary
        if (BCSouth == 0):  # Dirichlet
            Aub[0] = 1.0
            Aud[0] = 0.0
        elif (BCSouth == 1):  # Neumann
            Auc[0] = -1.0
            Aub[0] = 1.0
            Aud[0] = 0.0

        # Solve algebraic system
        u = TDMAsolver(Aua, Aub, Auc, Aud)

        # Compute rms error at the end of each time step
        rms_u = RMS_E(u, uold)

        if (rms_u < eps):
            if (dbg == 1):
                print("The simulation has converged in ", k, " iterations")
            break
        elif (abs(last_rms - rms_u) < min_step):
            if (dbg == 1):
                print("The simulation convergence is limited to ", rms_u, " in ", k, " iterations\n")
            break
        else:
            if (dbg == 1):
                print("k=", k, ": RMS U =", rms_u)
        uold[:] = u[:]
        k = k + 1
        last_rms = rms_u

    stop = time.time()
    if (dbg == 1):
        print("|Bench| Time (s): ", stop - start)

        print("result samples:")
        for idx in range(0, M, int(M / 10)):
            print("u[", idx, "] = ", u[idx])

def main():
    WI = 100
    WM = 64 * 1024
    BI = 10
    BM = 1 * 2048 * 2048

    print("|Bench| WARMUP ", WI, " iterations [len = ", WM,"]---")
    tv = time.time()
    print("|Bench| WARMUP Start: ", int(tv))

    for i in range(WI):
        solve(WM, 0) # hide logs

    tv = time.time()
    print("|Bench| WARMUP Stop: ", int(tv))

    time.sleep(3)

    print("|Bench| BENCHMARK ", BI, " iterations [", BM, "]---")
    tv = time.time()
    print("|Bench| BENCHMARK Start: ", int(tv))

    for i in range(BI):
        solve(BM, 1) # show logs #

    tv = time.time()
    print("|Bench| BENCHMARK Stop: ", int(tv))
    return 0

if __name__ == "__main__":
    main()
