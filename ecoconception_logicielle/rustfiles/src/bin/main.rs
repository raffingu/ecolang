/*
EcoInfo - Time & energy efficiency of programming languages in HPC
Copyright (C) 2020, CNRS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
extern crate ndarray;

use ndarray::*;

use std::time::Instant;
use std::time::{SystemTime, UNIX_EPOCH};
use std::{thread, time};

fn tdmasolver(a: &Array1<f64>, b: &Array1<f64>, c: &Array1<f64>, d: &Array1<f64>) -> Array1<f64> {
    //
    //TDMA solver, a b c d can be NumPy array type or Python list type.
    //refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    //
    let nf = b.len(); // number of equations
    let mut bc: Array1<f64> = Array::zeros(nf);
    let mut dc: Array1<f64> = Array::zeros(nf);
    let mut mc;

    bc.assign(&b); // copy the array
    dc.assign(&d); // copy the array

    for it in 1..nf {
        mc = a[it - 1] / bc[it - 1];
        bc[it] = bc[it] - mc * c[it - 1];
        dc[it] = dc[it] - mc * dc[it - 1];
    }

    bc[nf - 1] = dc[nf - 1] / bc[nf - 1];

    for il in (0..=(nf - 2)).rev() {
        bc[il] = (dc[il] - c[il] * bc[il + 1]) / bc[il];
    }
    bc
}

fn rms_e(x1: &Array1<f64>, x2: &Array1<f64>) -> f64 {
    // manual loop is faster (no alloc)
    let len = x1.len();
    let mut tot: f64 = 0.0;
    for i in 0..len {
        let a = x1[i] - x2[i];
        tot += a * a;
    }
    (tot / (len as f64)).sqrt()
}

fn solve(m: usize, dbg: usize) {
    let now = Instant::now();

    // Domain length
    let lx = 1.0_f64;

    // Grid sizes
    let dx = lx / (m as f64 - 1.0);

    // Reynolds number: Re = U L / nu
    let reynolds = 1.0_f64;

    // Pressure gradient
    let dpdx;
    // Velocity
    let c0;
    // Diffusivity
    let gamma;
    // Time step:
    let dt;
    // maximum number of iterations
    let maxiter;

    // Configuration
    let case = 1; //"diffusion";
                  // let case = 0; // "convection"

    //
    //     INITIALISATION
    //

    if case == 1 {
        // Pressure gradient
        dpdx = 1.0_f64;
        // Velocity
        c0 = 0.0_f64;
        // Diffusivity
        gamma = 1.0 / reynolds;
        // Time step:
        dt = 0.1;
        // maximum number of iterations
        maxiter = 1000;
    } else {
        println!("Invalid case = {}", case);
        return; // failure
    }

    // Precision
    let eps = 1e-9;

    // Print initial conditions:
    if dbg == 1 {
        println!("case : {}", case);
        println!("M  : {}", m);
        println!("dpdx : {}", dpdx);
        println!("dt : {}", dt);
        println!("eps: {}", eps);
    }

    // boundary conditions
    let bcnorth = 0;
    let bcsouth = 0;

    // Initialisation of variables
    let mut aua: Array1<f64> = Array::zeros(m - 1);
    let mut aub: Array1<f64> = Array::zeros(m);
    let mut auc: Array1<f64> = Array::zeros(m - 1);
    let mut aud: Array1<f64> = Array::zeros(m);

    let mut uold: Array1<f64> = Array::zeros(m);
    let mut u: Array1<f64> = Array::zeros(0);

    //
    // Convection fluxes
    //
    let fe = c0;
    let fw = c0;
    //
    // Diffusion fluxes
    //
    let de = gamma / dx;
    let dw = gamma / dx;
    //
    // Compute system coefficients
    //
    let ae = de + f64::max(-fe, 0.0);
    let aw = dw + f64::max(fw, 0.0);
    let ap = ae + aw + fe - fw;
    //
    // Assemble matrix coefficients
    //
    aua.fill(-aw);
    aub.slice_mut(s![1..(m - 1)]).fill(dx / dt + ap);
    auc.fill(-ae);

    // min step check:
    let mut rms_u;
    let mut last_rms = 1.0_f64;
    let min_step = eps / 10.0;

    // First iteration
    let mut k = 0;
    while k < maxiter {
        // Predictor step
        //
        // Convection fluxes
        //
        aud.assign(&(dx / dt * &uold + dpdx * dx));

        // Boundary conditions
        // North boundary
        if bcnorth == 0 {
            // Dirichlet
            aub[m - 1] = 1.0;
            aud[m - 1] = 0.0;
        } else if bcnorth == 1 {
            // Neumann
            aua[m - 2] = -1.0;
            aub[m - 1] = 1.0;
            aud[m - 1] = 0.0;
        }
        // South boundary
        if bcsouth == 0 {
            // Dirichlet
            aub[0] = 1.0;
            aud[0] = 0.0;
        } else if bcsouth == 1 {
            // Neumann
            auc[0] = -1.0;
            aub[0] = 1.0;
            aud[0] = 0.0;
        }

        // Solve algebraic system
        u = tdmasolver(&aua, &aub, &auc, &aud);

        // Compute rms error at the end of each time step
        rms_u = rms_e(&u, &uold);

        if f64::abs(rms_u) < eps {
            if dbg == 1 {
                println!("The simulation has converged in {} iterations", k);
            }
            break;
        } else if f64::abs(last_rms - rms_u) < min_step {
            if dbg == 1 {
                println!(
                    "The simulation convergence is limited to {} in {} iterations\n",
                    (last_rms - rms_u),
                    k
                );
            }
            break;
        } else {
            if dbg == 1 {
                println!("k={}: RMS U = {}", k, rms_u);
            }
        }
        uold.assign(&u);
        k += 1;
        last_rms = rms_u;
    }
    if dbg == 1 {
        println!("|Bench| Time (s): {}", now.elapsed().as_secs_f32());

        println!("result samples:");
        for idx in (0..m).step_by(m / 10) {
            println!("u[{}] = {}", idx, u[idx]);
        }
    }
}

fn main() {
    let wi = 100;
    let wm = 64 * 1024;
    let bi = 10;
    let bm = 1 * 2048 * 2048;

    println!("|Bench| WARMUP {} iterations [len = {}]---", wi, wm);

    let since_the_epoch = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    println!("|Bench| WARMUP Start: {}", since_the_epoch.as_secs());

    for _it in 0..wi {
        solve(wm, 0)
    }

    let since_the_epoch = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    println!("|Bench| WARMUP Stop: {}", since_the_epoch.as_secs());

    thread::sleep(time::Duration::from_millis(3000));

    println!("|Bench| BENCHMARK {} iterations [{}]---", bi, bm);
    let since_the_epoch = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    println!("|Bench| BENCHMARK Start: {}", since_the_epoch.as_secs());

    for _it in 0..bi {
        solve(bm, 1)
    }

    let since_the_epoch = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    println!("|Bench| BENCHMARK Stop: {}", since_the_epoch.as_secs());
}
