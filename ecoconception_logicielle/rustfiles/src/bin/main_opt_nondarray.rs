/*
EcoInfo - Time & energy efficiency of programming languages in HPC
Copyright (C) 2020, CNRS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
use std::time::Instant;
use std::time::{SystemTime, UNIX_EPOCH};
use std::{thread, time};

// use outputs for temporary arrays:
// bc = xc
// dc = tmp
fn tdmasolver(
    a: &[f64],
    b: &[f64],
    c: &[f64],
    d: &[f64],
    bc: &mut [f64],
    dc: &mut [f64],
) {
    //
    // TDMA solver, a b c d can be NumPy array type or Python list type.
    // refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    //
    let nf = b.len(); // number of equations

    bc.clone_from_slice(&b); // copy the array
    dc.clone_from_slice(&d); // copy the array

    let mut bcit = bc.iter_mut(); // bc [0..n]
    let mut dcit = dc.iter_mut(); // dc [0..n]

    let mut bcim1 = *bcit.next().unwrap_or(&mut 0.0_f64); // bc [0]
    let mut dcim1 = *dcit.next().unwrap_or(&mut 0.0_f64); // dc [0]

    // next() calls implies bcit/dcit points to [i+1]; but (a|c) point to [i]:

    for ((bci, dci), (aim1, cim1)) in (bcit.zip(dcit)).zip(a.iter().zip(c.iter())) {
        let mc: f64 = *aim1 / bcim1; // mc = a[it - 1] / bc[it - 1]
        *bci -= mc * *cim1;          // bc[it + 1] -= mc *  c[it]
        *dci -= mc * dcim1;          // dc[it + 1] -= mc * dc[it]
        // next iter:
        bcim1 = *bci;
        dcim1 = *dci;
    }
    /*
    unsafe {
        *bc.get_unchecked_mut(nf - 1) = dc.get_unchecked(nf - 1) / bc.get_unchecked(nf - 1);
    }
    */
     bc[nf - 1] = dc[nf - 1] / bc[nf - 1];

    let mut bcit = bc.iter_mut().rev();                   // bc [n..0]
    let mut bcip1 = *bcit.next().unwrap_or(&mut 0.0_f64); // bc [n]

    // next() calls implies bcit to [i-1]:
    // note: c[M - 1] so c rev() => M -2
    // note: bc or dc[M - 2] so c rev() skip(1) => M -2

    for (bci, (ci, dci)) in bcit.zip(c.iter().rev().zip(dc.iter().rev().skip(1))) {
        *bci = (*dci - *ci * bcip1) / *bci; // bc[it] = (dc[it] - c[it] * bc[it + 1]) / bc[it];

        // next iter:
        bcip1 = *bci;
    }
}

fn rms_e(x1: &[f64], x2: &[f64]) -> f64 {
    // manual loop is faster (no alloc)
    let len = x1.len();
    let mut tot: f64 = 0.0;

    for (x1i, x2i) in x1.iter().zip(x2.iter()) {
        let a = x1i - x2i;
        tot += a * a;
    }
    (tot / (len as f64)).sqrt()
}

fn solve(m: usize, dbg: usize) {
    let now = Instant::now();

    // Domain length
    let lx = 1.0_f64;

    // Grid sizes
    let dx = lx / (m as f64 - 1.0);

    // Reynolds number: Re = U L / nu
    let reynolds = 1.0_f64;

    // Pressure gradient
    let dpdx;
    // Velocity
    let c0;
    // Diffusivity
    let gamma;
    // Time step:
    let dt;
    // maximum number of iterations
    let maxiter;

    // Configuration
    let case = 1; //"diffusion";
                  // let case = 0; // "convection"

    //
    //     INITIALISATION
    //

    if case == 1 {
        // Pressure gradient
        dpdx = 1.0_f64;
        // Velocity
        c0 = 0.0_f64;
        // Diffusivity
        gamma = 1.0 / reynolds;
        // Time step:
        dt = 0.1;
        // maximum number of iterations
        maxiter = 1000;
    } else {
        println!("Invalid case = {}", case);
        return; // failure
    }

    // Precision
    let eps = 1e-9;

    // Print initial conditions:
    if dbg == 1 {
        println!("case : {}", case);
        println!("M  : {}", m);
        println!("dpdx : {}", dpdx);
        println!("dt : {}", dt);
        println!("eps: {}", eps);
    }

    // boundary conditions
    let bcnorth = 0;
    let bcsouth = 0;

    // Initialisation of variables
    let mut aua: Vec<f64> = vec![0_f64; m - 1];
    let mut aub: Vec<f64> = vec![0_f64; m];
    let mut auc: Vec<f64> = vec![0_f64; m - 1];
    let mut aud: Vec<f64> = vec![0_f64; m];

    let mut uold: Vec<f64> = vec![0_f64; m];
    let mut u: Vec<f64> = vec![0_f64; m];
    let mut tmp: Vec<f64> = vec![0_f64; m];

    //
    // Convection fluxes
    //
    let fe = c0;
    let fw = c0;
    //
    // Diffusion fluxes
    //
    let de = gamma / dx;
    let dw = gamma / dx;
    //
    // Compute system coefficients
    //
    let ae = de + f64::max(-fe, 0.0);
    let aw = dw + f64::max(fw, 0.0);
    let ap = ae + aw + fe - fw;
    //
    // Assemble matrix coefficients
    //
    aua.fill(-aw);
    // aub.slice_mut(s![1..(m - 1)]).fill(dx / dt + ap);
    let _ = &mut aub[1..m-1].fill(dx / dt + ap);
    auc.fill(-ae);

    // min step check:
    let mut rms_u;
    let mut last_rms = 1.0_f64;
    let min_step = eps / 10.0;

    // First iteration
    let mut k = 0;
    while k < maxiter {
        // Predictor step
        //
        // Convection fluxes
        //
        for (o, a) in  uold.iter().zip(aud.iter_mut()) {
           *a = (dx / dt) * *o + (dpdx * dx)
        }
        // aud.assign(&(dx / dt * &uold + dpdx * dx));

        // Boundary conditions
        // North boundary
        if bcnorth == 0 {
            // Dirichlet
            aub[m - 1] = 1.0;
            aud[m - 1] = 0.0;
        } else if bcnorth == 1 {
            // Neumann
            aua[m - 2] = -1.0;
            aub[m - 1] = 1.0;
            aud[m - 1] = 0.0;
        }
        // South boundary
        if bcsouth == 0 {
            // Dirichlet
            aub[0] = 1.0;
            aud[0] = 0.0;
        } else if bcsouth == 1 {
            // Neumann
            auc[0] = -1.0;
            aub[0] = 1.0;
            aud[0] = 0.0;
        }

        // Solve algebraic system
        // give 2 extra arrays (outputs) for preallocation
        tdmasolver(&aua, &aub, &auc, &aud, &mut u, &mut tmp);

        // Compute rms error at the end of each time step
        rms_u = rms_e(&u, &uold);

        if rms_u.abs() < eps {
            if dbg == 1 {
                println!("The simulation has converged in {} iterations", k);
            }
            break;
        } else if (last_rms - rms_u).abs() < min_step {
            if dbg == 1 {
                println!(
                    "The simulation convergence is limited to {} in {} iterations\n",
                    (last_rms - rms_u),
                    k
                );
            }
            break;
        } else {
            if dbg == 1 {
                println!("k={}: RMS U = {}", k, rms_u);
            }
        }
        for (u, o) in  u.iter().zip(uold.iter_mut()) {
            *o =  *u;
        }
        // uold.assign(&u);
        k += 1;
        last_rms = rms_u;
    }
    if dbg == 1 {
        println!("|Bench| Time (s): {}", now.elapsed().as_secs_f32());

        println!("result samples:");
        for idx in (0..m).step_by(m / 10) {
            println!("u[{}] = {}", idx, u[idx]);
        }
    }
}

fn main() {
    let wi = 100;
    let wm = 64 * 1024;
    let bi = 10;
    let bm = 1 * 2048 * 2048;

    println!("|Bench| WARMUP {} iterations [len = {}]---", wi, wm);

    let since_the_epoch = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    println!("|Bench| WARMUP Start: {}", since_the_epoch.as_secs());

    for _it in 0..wi {
        solve(wm, 0)
    }

    let since_the_epoch = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    println!("|Bench| WARMUP Stop: {}", since_the_epoch.as_secs());

    thread::sleep(time::Duration::from_millis(3000));

    println!("|Bench| BENCHMARK {} iterations [{}]---", bi, bm);
    let since_the_epoch = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    println!("|Bench| BENCHMARK Start: {}", since_the_epoch.as_secs());

    for _it in 0..bi {
        solve(bm, 1)
    }

    let since_the_epoch = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    println!("|Bench| BENCHMARK Stop: {}", since_the_epoch.as_secs());
}
