#!/bin/bash
# Needs sudo

# Use US Locale:
export LANG=C

for i in $(ls /sys/devices/system/cpu/cpu*/online); do 
    ENABLE=`cat ${i}`

    if [ "${ENABLE}" -ne "1" ]
    then
        echo "Enabling core ${i}."
        echo 1 > ${i};
    fi
done

