Python 3.9.2
Running main1D_cb_numpy_opt.py
/root/ecoconception_logicielle/pyfiles/main1D_cb_numpy_opt.py:126: DeprecationWarning: `np.float` is a deprecated alias for the builtin `float`. To silence this warning, use `float` by itself. Doing this will not modify any behavior and is safe. If you specifically wanted the numpy scalar type, use `np.float64` here.
Deprecated in NumPy 1.20; for more details and guidance: https://numpy.org/devdocs/release/1.20.0-notes.html#deprecations
  Aua = np.zeros([M - 1], dtype=np.float)
/root/ecoconception_logicielle/pyfiles/main1D_cb_numpy_opt.py:127: DeprecationWarning: `np.float` is a deprecated alias for the builtin `float`. To silence this warning, use `float` by itself. Doing this will not modify any behavior and is safe. If you specifically wanted the numpy scalar type, use `np.float64` here.
Deprecated in NumPy 1.20; for more details and guidance: https://numpy.org/devdocs/release/1.20.0-notes.html#deprecations
  Aub = np.zeros([M], dtype=np.float)
/root/ecoconception_logicielle/pyfiles/main1D_cb_numpy_opt.py:128: DeprecationWarning: `np.float` is a deprecated alias for the builtin `float`. To silence this warning, use `float` by itself. Doing this will not modify any behavior and is safe. If you specifically wanted the numpy scalar type, use `np.float64` here.
Deprecated in NumPy 1.20; for more details and guidance: https://numpy.org/devdocs/release/1.20.0-notes.html#deprecations
  Auc = np.zeros([M - 1], dtype=np.float)
/root/ecoconception_logicielle/pyfiles/main1D_cb_numpy_opt.py:129: DeprecationWarning: `np.float` is a deprecated alias for the builtin `float`. To silence this warning, use `float` by itself. Doing this will not modify any behavior and is safe. If you specifically wanted the numpy scalar type, use `np.float64` here.
Deprecated in NumPy 1.20; for more details and guidance: https://numpy.org/devdocs/release/1.20.0-notes.html#deprecations
  Aud = np.zeros([M], dtype=np.float)
/root/ecoconception_logicielle/pyfiles/main1D_cb_numpy_opt.py:131: DeprecationWarning: `np.float` is a deprecated alias for the builtin `float`. To silence this warning, use `float` by itself. Doing this will not modify any behavior and is safe. If you specifically wanted the numpy scalar type, use `np.float64` here.
Deprecated in NumPy 1.20; for more details and guidance: https://numpy.org/devdocs/release/1.20.0-notes.html#deprecations
  uold = np.zeros([M], dtype=np.float)
/root/ecoconception_logicielle/pyfiles/main1D_cb_numpy_opt.py:132: DeprecationWarning: `np.float` is a deprecated alias for the builtin `float`. To silence this warning, use `float` by itself. Doing this will not modify any behavior and is safe. If you specifically wanted the numpy scalar type, use `np.float64` here.
Deprecated in NumPy 1.20; for more details and guidance: https://numpy.org/devdocs/release/1.20.0-notes.html#deprecations
  u    = np.zeros([M], dtype=np.float)
/root/ecoconception_logicielle/pyfiles/main1D_cb_numpy_opt.py:133: DeprecationWarning: `np.float` is a deprecated alias for the builtin `float`. To silence this warning, use `float` by itself. Doing this will not modify any behavior and is safe. If you specifically wanted the numpy scalar type, use `np.float64` here.
Deprecated in NumPy 1.20; for more details and guidance: https://numpy.org/devdocs/release/1.20.0-notes.html#deprecations
  tmp  = np.zeros([M], dtype=np.float)
|Bench| WARMUP  5  iterations [len =  65536 ]---
|Bench| WARMUP Start:  1641465063
|Bench| WARMUP Stop:  1641465079
|Bench| BENCHMARK  1  iterations [ 4194304 ]---
|Bench| BENCHMARK Start:  1641465082
case :  diffusion
M  :  4194304
dx :  2.384186359449949e-07
dt :  0.1
eps:  1e-09
k= 0 : RMS U = 0.04541927413192137
k= 1 : RMS U = 0.02280637001002756
k= 2 : RMS U = 0.011477009394214937
k= 3 : RMS U = 0.005776142880309186
k= 4 : RMS U = 0.002907023799080043
k= 5 : RMS U = 0.001463050464303699
k= 6 : RMS U = 0.0007363258166587269
k= 7 : RMS U = 0.00037057895241738643
k= 8 : RMS U = 0.00018650542583187356
k= 9 : RMS U = 9.386467751701586e-05
k= 10 : RMS U = 4.724032904260682e-05
k= 11 : RMS U = 2.377517024015427e-05
k= 12 : RMS U = 1.1965596603488468e-05
k= 13 : RMS U = 6.0220599967910784e-06
k= 14 : RMS U = 3.0307896925693134e-06
k= 15 : RMS U = 1.5253395108420217e-06
k= 16 : RMS U = 7.676747338788836e-07
k= 17 : RMS U = 3.863562637494926e-07
k= 18 : RMS U = 1.9444585432654276e-07
k= 19 : RMS U = 9.78609424993671e-08
k= 20 : RMS U = 4.925157494958241e-08
k= 21 : RMS U = 2.4787401652848977e-08
k= 22 : RMS U = 1.2475028201778152e-08
k= 23 : RMS U = 6.278447159441265e-09
k= 24 : RMS U = 3.1598218036193177e-09
k= 25 : RMS U = 1.5902840669814338e-09
The simulation has converged in  26  iterations
|Bench| Time (s):  207.45493912696838
result samples:
u[ 0 ] =  -1.1920951671438927e-07
u[ 419430 ] =  0.04499992482473523
u[ 838860 ] =  0.07999993543679544
u[ 1258290 ] =  0.10499994630079647
u[ 1677720 ] =  0.11999995391834647
u[ 2097150 ] =  0.12499996426193023
u[ 2516580 ] =  0.11999998521222201
u[ 2936010 ] =  0.10500001951243966
u[ 3355440 ] =  0.08000007235141963
u[ 3774870 ] =  0.045000144025146564
u[ 4194300 ] =  2.3841839751250414e-07
|Bench| BENCHMARK Stop:  1641465289
