 |Bench| WARMUP          100  iterations [len =        65536 ]---
 |Bench| WARMUP Start:   1641463824
 |Bench| WARMUP Stop:   1641463827
 |Bench| BENCHMARK           10  iterations [len =      4194304 ]---
 |Bench| BENCHMARK Start:   1641463830
 case :            0
 M  :      4194304
 dx :    2.3841863594499491E-007
 dt :   0.10000000000000001     
 eps:    1.0000000000000001E-009
 k=           0 : RMS U =   4.5419274131920109E-002
 k=           1 : RMS U =   2.2806370010027978E-002
 k=           2 : RMS U =   1.1477009394214021E-002
 k=           3 : RMS U =   5.7761428803093763E-003
 k=           4 : RMS U =   2.9070237990799531E-003
 k=           5 : RMS U =   1.4630504643037324E-003
 k=           6 : RMS U =   7.3632581665872190E-004
 k=           7 : RMS U =   3.7057895241739483E-004
 k=           8 : RMS U =   1.8650542583186809E-004
 k=           9 : RMS U =   9.3864677517016686E-005
 k=          10 : RMS U =   4.7240329042606891E-005
 k=          11 : RMS U =   2.3775170240152950E-005
 k=          12 : RMS U =   1.1965596603487778E-005
 k=          13 : RMS U =   6.0220599967909861E-006
 k=          14 : RMS U =   3.0307896925692677E-006
 k=          15 : RMS U =   1.5253395108420100E-006
 k=          16 : RMS U =   7.6767473387889452E-007
 k=          17 : RMS U =   3.8635626374947923E-007
 k=          18 : RMS U =   1.9444585432654678E-007
 k=          19 : RMS U =   9.7860942499366044E-008
 k=          20 : RMS U =   4.9251574949580625E-008
 k=          21 : RMS U =   2.4787401652849136E-008
 k=          22 : RMS U =   1.2475028201777867E-008
 k=          23 : RMS U =   6.2784471594409989E-009
 k=          24 : RMS U =   3.1598218036194538E-009
 k=          25 : RMS U =   1.5902840669815077E-009
 The simulation has converged in           26  iterations
 |Bench| Time (s):    6.74598885    
 result samples:
 u[           1 ] =   -1.1920951671438927E-007
 u[      419431 ] =    4.4999924824735228E-002
 u[      838861 ] =    7.9999935436795441E-002
 u[     1258291 ] =   0.10499994630079647     
 u[     1677721 ] =   0.11999995391834647     
 u[     2097151 ] =   0.12499996426193023     
 u[     2516581 ] =   0.11999998521222201     
 u[     2936011 ] =   0.10500001951243966     
 u[     3355441 ] =    8.0000072351419635E-002
 u[     3774871 ] =    4.5000144025146564E-002
 u[     4194301 ] =    2.3841839751250414E-007
 case :            0
 M  :      4194304
 dx :    2.3841863594499491E-007
 dt :   0.10000000000000001     
 eps:    1.0000000000000001E-009
 k=           0 : RMS U =   4.5419274131920109E-002
 k=           1 : RMS U =   2.2806370010027978E-002
 k=           2 : RMS U =   1.1477009394214021E-002
 k=           3 : RMS U =   5.7761428803093763E-003
 k=           4 : RMS U =   2.9070237990799531E-003
 k=           5 : RMS U =   1.4630504643037324E-003
 k=           6 : RMS U =   7.3632581665872190E-004
 k=           7 : RMS U =   3.7057895241739483E-004
 k=           8 : RMS U =   1.8650542583186809E-004
 k=           9 : RMS U =   9.3864677517016686E-005
 k=          10 : RMS U =   4.7240329042606891E-005
 k=          11 : RMS U =   2.3775170240152950E-005
 k=          12 : RMS U =   1.1965596603487778E-005
 k=          13 : RMS U =   6.0220599967909861E-006
 k=          14 : RMS U =   3.0307896925692677E-006
 k=          15 : RMS U =   1.5253395108420100E-006
 k=          16 : RMS U =   7.6767473387889452E-007
 k=          17 : RMS U =   3.8635626374947923E-007
 k=          18 : RMS U =   1.9444585432654678E-007
 k=          19 : RMS U =   9.7860942499366044E-008
 k=          20 : RMS U =   4.9251574949580625E-008
 k=          21 : RMS U =   2.4787401652849136E-008
 k=          22 : RMS U =   1.2475028201777867E-008
 k=          23 : RMS U =   6.2784471594409989E-009
 k=          24 : RMS U =   3.1598218036194538E-009
 k=          25 : RMS U =   1.5902840669815077E-009
 The simulation has converged in           26  iterations
 |Bench| Time (s):    6.73506308    
 result samples:
 u[           1 ] =   -1.1920951671438927E-007
 u[      419431 ] =    4.4999924824735228E-002
 u[      838861 ] =    7.9999935436795441E-002
 u[     1258291 ] =   0.10499994630079647     
 u[     1677721 ] =   0.11999995391834647     
 u[     2097151 ] =   0.12499996426193023     
 u[     2516581 ] =   0.11999998521222201     
 u[     2936011 ] =   0.10500001951243966     
 u[     3355441 ] =    8.0000072351419635E-002
 u[     3774871 ] =    4.5000144025146564E-002
 u[     4194301 ] =    2.3841839751250414E-007
 case :            0
 M  :      4194304
 dx :    2.3841863594499491E-007
 dt :   0.10000000000000001     
 eps:    1.0000000000000001E-009
 k=           0 : RMS U =   4.5419274131920109E-002
 k=           1 : RMS U =   2.2806370010027978E-002
 k=           2 : RMS U =   1.1477009394214021E-002
 k=           3 : RMS U =   5.7761428803093763E-003
 k=           4 : RMS U =   2.9070237990799531E-003
 k=           5 : RMS U =   1.4630504643037324E-003
 k=           6 : RMS U =   7.3632581665872190E-004
 k=           7 : RMS U =   3.7057895241739483E-004
 k=           8 : RMS U =   1.8650542583186809E-004
 k=           9 : RMS U =   9.3864677517016686E-005
 k=          10 : RMS U =   4.7240329042606891E-005
 k=          11 : RMS U =   2.3775170240152950E-005
 k=          12 : RMS U =   1.1965596603487778E-005
 k=          13 : RMS U =   6.0220599967909861E-006
 k=          14 : RMS U =   3.0307896925692677E-006
 k=          15 : RMS U =   1.5253395108420100E-006
 k=          16 : RMS U =   7.6767473387889452E-007
 k=          17 : RMS U =   3.8635626374947923E-007
 k=          18 : RMS U =   1.9444585432654678E-007
 k=          19 : RMS U =   9.7860942499366044E-008
 k=          20 : RMS U =   4.9251574949580625E-008
 k=          21 : RMS U =   2.4787401652849136E-008
 k=          22 : RMS U =   1.2475028201777867E-008
 k=          23 : RMS U =   6.2784471594409989E-009
 k=          24 : RMS U =   3.1598218036194538E-009
 k=          25 : RMS U =   1.5902840669815077E-009
 The simulation has converged in           26  iterations
 |Bench| Time (s):    6.69969702    
 result samples:
 u[           1 ] =   -1.1920951671438927E-007
 u[      419431 ] =    4.4999924824735228E-002
 u[      838861 ] =    7.9999935436795441E-002
 u[     1258291 ] =   0.10499994630079647     
 u[     1677721 ] =   0.11999995391834647     
 u[     2097151 ] =   0.12499996426193023     
 u[     2516581 ] =   0.11999998521222201     
 u[     2936011 ] =   0.10500001951243966     
 u[     3355441 ] =    8.0000072351419635E-002
 u[     3774871 ] =    4.5000144025146564E-002
 u[     4194301 ] =    2.3841839751250414E-007
 case :            0
 M  :      4194304
 dx :    2.3841863594499491E-007
 dt :   0.10000000000000001     
 eps:    1.0000000000000001E-009
 k=           0 : RMS U =   4.5419274131920109E-002
 k=           1 : RMS U =   2.2806370010027978E-002
 k=           2 : RMS U =   1.1477009394214021E-002
 k=           3 : RMS U =   5.7761428803093763E-003
 k=           4 : RMS U =   2.9070237990799531E-003
 k=           5 : RMS U =   1.4630504643037324E-003
 k=           6 : RMS U =   7.3632581665872190E-004
 k=           7 : RMS U =   3.7057895241739483E-004
 k=           8 : RMS U =   1.8650542583186809E-004
 k=           9 : RMS U =   9.3864677517016686E-005
 k=          10 : RMS U =   4.7240329042606891E-005
 k=          11 : RMS U =   2.3775170240152950E-005
 k=          12 : RMS U =   1.1965596603487778E-005
 k=          13 : RMS U =   6.0220599967909861E-006
 k=          14 : RMS U =   3.0307896925692677E-006
 k=          15 : RMS U =   1.5253395108420100E-006
 k=          16 : RMS U =   7.6767473387889452E-007
 k=          17 : RMS U =   3.8635626374947923E-007
 k=          18 : RMS U =   1.9444585432654678E-007
 k=          19 : RMS U =   9.7860942499366044E-008
 k=          20 : RMS U =   4.9251574949580625E-008
 k=          21 : RMS U =   2.4787401652849136E-008
 k=          22 : RMS U =   1.2475028201777867E-008
 k=          23 : RMS U =   6.2784471594409989E-009
 k=          24 : RMS U =   3.1598218036194538E-009
 k=          25 : RMS U =   1.5902840669815077E-009
 The simulation has converged in           26  iterations
 |Bench| Time (s):    6.67836905    
 result samples:
 u[           1 ] =   -1.1920951671438927E-007
 u[      419431 ] =    4.4999924824735228E-002
 u[      838861 ] =    7.9999935436795441E-002
 u[     1258291 ] =   0.10499994630079647     
 u[     1677721 ] =   0.11999995391834647     
 u[     2097151 ] =   0.12499996426193023     
 u[     2516581 ] =   0.11999998521222201     
 u[     2936011 ] =   0.10500001951243966     
 u[     3355441 ] =    8.0000072351419635E-002
 u[     3774871 ] =    4.5000144025146564E-002
 u[     4194301 ] =    2.3841839751250414E-007
 case :            0
 M  :      4194304
 dx :    2.3841863594499491E-007
 dt :   0.10000000000000001     
 eps:    1.0000000000000001E-009
 k=           0 : RMS U =   4.5419274131920109E-002
 k=           1 : RMS U =   2.2806370010027978E-002
 k=           2 : RMS U =   1.1477009394214021E-002
 k=           3 : RMS U =   5.7761428803093763E-003
 k=           4 : RMS U =   2.9070237990799531E-003
 k=           5 : RMS U =   1.4630504643037324E-003
 k=           6 : RMS U =   7.3632581665872190E-004
 k=           7 : RMS U =   3.7057895241739483E-004
 k=           8 : RMS U =   1.8650542583186809E-004
 k=           9 : RMS U =   9.3864677517016686E-005
 k=          10 : RMS U =   4.7240329042606891E-005
 k=          11 : RMS U =   2.3775170240152950E-005
 k=          12 : RMS U =   1.1965596603487778E-005
 k=          13 : RMS U =   6.0220599967909861E-006
 k=          14 : RMS U =   3.0307896925692677E-006
 k=          15 : RMS U =   1.5253395108420100E-006
 k=          16 : RMS U =   7.6767473387889452E-007
 k=          17 : RMS U =   3.8635626374947923E-007
 k=          18 : RMS U =   1.9444585432654678E-007
 k=          19 : RMS U =   9.7860942499366044E-008
 k=          20 : RMS U =   4.9251574949580625E-008
 k=          21 : RMS U =   2.4787401652849136E-008
 k=          22 : RMS U =   1.2475028201777867E-008
 k=          23 : RMS U =   6.2784471594409989E-009
 k=          24 : RMS U =   3.1598218036194538E-009
 k=          25 : RMS U =   1.5902840669815077E-009
 The simulation has converged in           26  iterations
 |Bench| Time (s):    6.69731379    
 result samples:
 u[           1 ] =   -1.1920951671438927E-007
 u[      419431 ] =    4.4999924824735228E-002
 u[      838861 ] =    7.9999935436795441E-002
 u[     1258291 ] =   0.10499994630079647     
 u[     1677721 ] =   0.11999995391834647     
 u[     2097151 ] =   0.12499996426193023     
 u[     2516581 ] =   0.11999998521222201     
 u[     2936011 ] =   0.10500001951243966     
 u[     3355441 ] =    8.0000072351419635E-002
 u[     3774871 ] =    4.5000144025146564E-002
 u[     4194301 ] =    2.3841839751250414E-007
 case :            0
 M  :      4194304
 dx :    2.3841863594499491E-007
 dt :   0.10000000000000001     
 eps:    1.0000000000000001E-009
 k=           0 : RMS U =   4.5419274131920109E-002
 k=           1 : RMS U =   2.2806370010027978E-002
 k=           2 : RMS U =   1.1477009394214021E-002
 k=           3 : RMS U =   5.7761428803093763E-003
 k=           4 : RMS U =   2.9070237990799531E-003
 k=           5 : RMS U =   1.4630504643037324E-003
 k=           6 : RMS U =   7.3632581665872190E-004
 k=           7 : RMS U =   3.7057895241739483E-004
 k=           8 : RMS U =   1.8650542583186809E-004
 k=           9 : RMS U =   9.3864677517016686E-005
 k=          10 : RMS U =   4.7240329042606891E-005
 k=          11 : RMS U =   2.3775170240152950E-005
 k=          12 : RMS U =   1.1965596603487778E-005
 k=          13 : RMS U =   6.0220599967909861E-006
 k=          14 : RMS U =   3.0307896925692677E-006
 k=          15 : RMS U =   1.5253395108420100E-006
 k=          16 : RMS U =   7.6767473387889452E-007
 k=          17 : RMS U =   3.8635626374947923E-007
 k=          18 : RMS U =   1.9444585432654678E-007
 k=          19 : RMS U =   9.7860942499366044E-008
 k=          20 : RMS U =   4.9251574949580625E-008
 k=          21 : RMS U =   2.4787401652849136E-008
 k=          22 : RMS U =   1.2475028201777867E-008
 k=          23 : RMS U =   6.2784471594409989E-009
 k=          24 : RMS U =   3.1598218036194538E-009
 k=          25 : RMS U =   1.5902840669815077E-009
 The simulation has converged in           26  iterations
 |Bench| Time (s):    6.68158102    
 result samples:
 u[           1 ] =   -1.1920951671438927E-007
 u[      419431 ] =    4.4999924824735228E-002
 u[      838861 ] =    7.9999935436795441E-002
 u[     1258291 ] =   0.10499994630079647     
 u[     1677721 ] =   0.11999995391834647     
 u[     2097151 ] =   0.12499996426193023     
 u[     2516581 ] =   0.11999998521222201     
 u[     2936011 ] =   0.10500001951243966     
 u[     3355441 ] =    8.0000072351419635E-002
 u[     3774871 ] =    4.5000144025146564E-002
 u[     4194301 ] =    2.3841839751250414E-007
 case :            0
 M  :      4194304
 dx :    2.3841863594499491E-007
 dt :   0.10000000000000001     
 eps:    1.0000000000000001E-009
 k=           0 : RMS U =   4.5419274131920109E-002
 k=           1 : RMS U =   2.2806370010027978E-002
 k=           2 : RMS U =   1.1477009394214021E-002
 k=           3 : RMS U =   5.7761428803093763E-003
 k=           4 : RMS U =   2.9070237990799531E-003
 k=           5 : RMS U =   1.4630504643037324E-003
 k=           6 : RMS U =   7.3632581665872190E-004
 k=           7 : RMS U =   3.7057895241739483E-004
 k=           8 : RMS U =   1.8650542583186809E-004
 k=           9 : RMS U =   9.3864677517016686E-005
 k=          10 : RMS U =   4.7240329042606891E-005
 k=          11 : RMS U =   2.3775170240152950E-005
 k=          12 : RMS U =   1.1965596603487778E-005
 k=          13 : RMS U =   6.0220599967909861E-006
 k=          14 : RMS U =   3.0307896925692677E-006
 k=          15 : RMS U =   1.5253395108420100E-006
 k=          16 : RMS U =   7.6767473387889452E-007
 k=          17 : RMS U =   3.8635626374947923E-007
 k=          18 : RMS U =   1.9444585432654678E-007
 k=          19 : RMS U =   9.7860942499366044E-008
 k=          20 : RMS U =   4.9251574949580625E-008
 k=          21 : RMS U =   2.4787401652849136E-008
 k=          22 : RMS U =   1.2475028201777867E-008
 k=          23 : RMS U =   6.2784471594409989E-009
 k=          24 : RMS U =   3.1598218036194538E-009
 k=          25 : RMS U =   1.5902840669815077E-009
 The simulation has converged in           26  iterations
 |Bench| Time (s):    6.68445206    
 result samples:
 u[           1 ] =   -1.1920951671438927E-007
 u[      419431 ] =    4.4999924824735228E-002
 u[      838861 ] =    7.9999935436795441E-002
 u[     1258291 ] =   0.10499994630079647     
 u[     1677721 ] =   0.11999995391834647     
 u[     2097151 ] =   0.12499996426193023     
 u[     2516581 ] =   0.11999998521222201     
 u[     2936011 ] =   0.10500001951243966     
 u[     3355441 ] =    8.0000072351419635E-002
 u[     3774871 ] =    4.5000144025146564E-002
 u[     4194301 ] =    2.3841839751250414E-007
 case :            0
 M  :      4194304
 dx :    2.3841863594499491E-007
 dt :   0.10000000000000001     
 eps:    1.0000000000000001E-009
 k=           0 : RMS U =   4.5419274131920109E-002
 k=           1 : RMS U =   2.2806370010027978E-002
 k=           2 : RMS U =   1.1477009394214021E-002
 k=           3 : RMS U =   5.7761428803093763E-003
 k=           4 : RMS U =   2.9070237990799531E-003
 k=           5 : RMS U =   1.4630504643037324E-003
 k=           6 : RMS U =   7.3632581665872190E-004
 k=           7 : RMS U =   3.7057895241739483E-004
 k=           8 : RMS U =   1.8650542583186809E-004
 k=           9 : RMS U =   9.3864677517016686E-005
 k=          10 : RMS U =   4.7240329042606891E-005
 k=          11 : RMS U =   2.3775170240152950E-005
 k=          12 : RMS U =   1.1965596603487778E-005
 k=          13 : RMS U =   6.0220599967909861E-006
 k=          14 : RMS U =   3.0307896925692677E-006
 k=          15 : RMS U =   1.5253395108420100E-006
 k=          16 : RMS U =   7.6767473387889452E-007
 k=          17 : RMS U =   3.8635626374947923E-007
 k=          18 : RMS U =   1.9444585432654678E-007
 k=          19 : RMS U =   9.7860942499366044E-008
 k=          20 : RMS U =   4.9251574949580625E-008
 k=          21 : RMS U =   2.4787401652849136E-008
 k=          22 : RMS U =   1.2475028201777867E-008
 k=          23 : RMS U =   6.2784471594409989E-009
 k=          24 : RMS U =   3.1598218036194538E-009
 k=          25 : RMS U =   1.5902840669815077E-009
 The simulation has converged in           26  iterations
 |Bench| Time (s):    6.68832397    
 result samples:
 u[           1 ] =   -1.1920951671438927E-007
 u[      419431 ] =    4.4999924824735228E-002
 u[      838861 ] =    7.9999935436795441E-002
 u[     1258291 ] =   0.10499994630079647     
 u[     1677721 ] =   0.11999995391834647     
 u[     2097151 ] =   0.12499996426193023     
 u[     2516581 ] =   0.11999998521222201     
 u[     2936011 ] =   0.10500001951243966     
 u[     3355441 ] =    8.0000072351419635E-002
 u[     3774871 ] =    4.5000144025146564E-002
 u[     4194301 ] =    2.3841839751250414E-007
 case :            0
 M  :      4194304
 dx :    2.3841863594499491E-007
 dt :   0.10000000000000001     
 eps:    1.0000000000000001E-009
 k=           0 : RMS U =   4.5419274131920109E-002
 k=           1 : RMS U =   2.2806370010027978E-002
 k=           2 : RMS U =   1.1477009394214021E-002
 k=           3 : RMS U =   5.7761428803093763E-003
 k=           4 : RMS U =   2.9070237990799531E-003
 k=           5 : RMS U =   1.4630504643037324E-003
 k=           6 : RMS U =   7.3632581665872190E-004
 k=           7 : RMS U =   3.7057895241739483E-004
 k=           8 : RMS U =   1.8650542583186809E-004
 k=           9 : RMS U =   9.3864677517016686E-005
 k=          10 : RMS U =   4.7240329042606891E-005
 k=          11 : RMS U =   2.3775170240152950E-005
 k=          12 : RMS U =   1.1965596603487778E-005
 k=          13 : RMS U =   6.0220599967909861E-006
 k=          14 : RMS U =   3.0307896925692677E-006
 k=          15 : RMS U =   1.5253395108420100E-006
 k=          16 : RMS U =   7.6767473387889452E-007
 k=          17 : RMS U =   3.8635626374947923E-007
 k=          18 : RMS U =   1.9444585432654678E-007
 k=          19 : RMS U =   9.7860942499366044E-008
 k=          20 : RMS U =   4.9251574949580625E-008
 k=          21 : RMS U =   2.4787401652849136E-008
 k=          22 : RMS U =   1.2475028201777867E-008
 k=          23 : RMS U =   6.2784471594409989E-009
 k=          24 : RMS U =   3.1598218036194538E-009
 k=          25 : RMS U =   1.5902840669815077E-009
 The simulation has converged in           26  iterations
 |Bench| Time (s):    6.70364904    
 result samples:
 u[           1 ] =   -1.1920951671438927E-007
 u[      419431 ] =    4.4999924824735228E-002
 u[      838861 ] =    7.9999935436795441E-002
 u[     1258291 ] =   0.10499994630079647     
 u[     1677721 ] =   0.11999995391834647     
 u[     2097151 ] =   0.12499996426193023     
 u[     2516581 ] =   0.11999998521222201     
 u[     2936011 ] =   0.10500001951243966     
 u[     3355441 ] =    8.0000072351419635E-002
 u[     3774871 ] =    4.5000144025146564E-002
 u[     4194301 ] =    2.3841839751250414E-007
 case :            0
 M  :      4194304
 dx :    2.3841863594499491E-007
 dt :   0.10000000000000001     
 eps:    1.0000000000000001E-009
 k=           0 : RMS U =   4.5419274131920109E-002
 k=           1 : RMS U =   2.2806370010027978E-002
 k=           2 : RMS U =   1.1477009394214021E-002
 k=           3 : RMS U =   5.7761428803093763E-003
 k=           4 : RMS U =   2.9070237990799531E-003
 k=           5 : RMS U =   1.4630504643037324E-003
 k=           6 : RMS U =   7.3632581665872190E-004
 k=           7 : RMS U =   3.7057895241739483E-004
 k=           8 : RMS U =   1.8650542583186809E-004
 k=           9 : RMS U =   9.3864677517016686E-005
 k=          10 : RMS U =   4.7240329042606891E-005
 k=          11 : RMS U =   2.3775170240152950E-005
 k=          12 : RMS U =   1.1965596603487778E-005
 k=          13 : RMS U =   6.0220599967909861E-006
 k=          14 : RMS U =   3.0307896925692677E-006
 k=          15 : RMS U =   1.5253395108420100E-006
 k=          16 : RMS U =   7.6767473387889452E-007
 k=          17 : RMS U =   3.8635626374947923E-007
 k=          18 : RMS U =   1.9444585432654678E-007
 k=          19 : RMS U =   9.7860942499366044E-008
 k=          20 : RMS U =   4.9251574949580625E-008
 k=          21 : RMS U =   2.4787401652849136E-008
 k=          22 : RMS U =   1.2475028201777867E-008
 k=          23 : RMS U =   6.2784471594409989E-009
 k=          24 : RMS U =   3.1598218036194538E-009
 k=          25 : RMS U =   1.5902840669815077E-009
 The simulation has converged in           26  iterations
 |Bench| Time (s):    5.33580303    
 result samples:
 u[           1 ] =   -1.1920951671438927E-007
 u[      419431 ] =    4.4999924824735228E-002
 u[      838861 ] =    7.9999935436795441E-002
 u[     1258291 ] =   0.10499994630079647     
 u[     1677721 ] =   0.11999995391834647     
 u[     2097151 ] =   0.12499996426193023     
 u[     2516581 ] =   0.11999998521222201     
 u[     2936011 ] =   0.10500001951243966     
 u[     3355441 ] =    8.0000072351419635E-002
 u[     3774871 ] =    4.5000144025146564E-002
 u[     4194301 ] =    2.3841839751250414E-007
 |Bench| BENCHMARK Stop:   1641463896
