#!/bin/bash

DO_VMSTAT=0

DATE_FMT="+%s"                  # used by getwatts
DATE_FMT="+%Y-%m-%dT%H:%M:%S"   # used by kwollect

SLEEP=3

# TRAP: Do not leave children jobs running if the shell has been cancelled
cleanup_trap() {
    CHILDREN_PIDS=$(jobs -p)
    if [ -n "$CHILDREN_PIDS" ]
    then
        trap - EXIT
        echo -e "SHELL cancelled, stopping $CHILDREN_PIDS"
        # we may try to send only TERM before a pause and a last loop with KILL signal ?
        kill $CHILDREN_PIDS

        echo -e "SHELL cancelled, waiting on $CHILDREN_PIDS"
        # wait for all pids
        for pid in $CHILDREN_PIDS; do
            wait $pid
        done

        CHILDREN_PIDS=$(jobs -p)
        if [ -n "$CHILDREN_PIDS" ]
        then
            echo -e "SHELL cancelled, killing $CHILDREN_PIDS"
            kill -9 $CHILDREN_PIDS
        fi
  fi
}
trap cleanup_trap EXIT


# HERE BEGINS THE SCRIPT

# Use US Locale:
export LANG=C

if [ -z "$1" ]
  then
    echo "No argument 1 (CMD) supplied"
    exit 1
fi


processId="1D" # all benchmarks contains '1D'

# Check if service is running:
PID=`ps -e | grep "${processId}" | grep -v "grep" | grep -v "run-bench*.sh" | awk '{print $1}'`
#echo "PID: '${PID}'"

N_PID=`echo "${PID}" | wc -w`

if [ "${N_PID}" -ne "0" ]
then
    echo "Process '${processId}' (PID = ${PID}) is already running ..."
    exit 1
fi



CMD="$1"
DIR=`dirname $1`
LABEL=`basename $1`
HOSTNAME=`hostname`

dir=./results/${HOSTNAME}-$(date +"%Y_%m_%d")/${LABEL}
mkdir -p $dir
echo "Store results in $dir"

logExt=txt
vmFile=${dir}/vm_${LABEL}.${logExt}
logFile=${dir}/result_${LABEL}


echo "---------------------------------------"
echo "Run benchmarks on command '$CMD' on 1 CPU core"
echo "---"


if [ $DO_VMSTAT -eq 1 ]
then
    # stop any vmstat
    killall -q vmstat

    echo "Start vmstat (1s) in background (CPU 0)..."
    taskset -c 0 vmstat -t 1 2>&1 > ${vmFile} &
fi



# Single job:
echo "---"
echo "Run Benchmark on 1 CPU (monothread) ... '$CMD'"

# Log timestamps (needed by getwatts)
# Start monothread:
START_DATE=$(date $DATE_FMT)
echo "START_DATE: $START_DATE"
echo "$START_DATE" >> results/timestamp

echo "Sleeping for $SLEEP seconds."
sleep $SLEEP

echo "Run process (CPU 1)..."
taskset -c 1 $CMD > ${logFile}_single.${logExt} 2>&1

echo "Results (monothread):"
grep -h "|Bench|" ${logFile}_single.${logExt}

echo "---"


# Log timestamps (needed by getwatts)
# End:
echo "Sleeping for $SLEEP seconds."
sleep $SLEEP

END_DATE=$(date $DATE_FMT)
echo "START_DATE: $START_DATE"
echo "END_DATE: $END_DATE"
echo "$END_DATE" >> results/timestamp


if [ $DO_VMSTAT -eq 1 ]
then
    # stop any vmstat
    killall -q vmstat
fi

trap - EXIT
echo "Done."
echo "---------------------------------------"

