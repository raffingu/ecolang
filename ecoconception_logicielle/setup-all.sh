#!/bin/bash

# Use US Locale:
export LANG=C


echo "setup-all: start ..."

# C:
./setup-bench.sh cfiles/1DC

# Fortran:
./setup-bench.sh ffiles/1Dfortran

# Java
./setup-bench.sh javafiles/1Djava

# Julia:
./setup-bench.sh jlfiles/1Djulia

# Rust:
./setup-bench.sh rustfiles/1Drust

# Go:
./setup-bench.sh gofiles/1Dgo

# Last is python (long and can be interrupted):
# Python:
./setup-bench.sh pyfiles/1Dpython_numpy

echo "setup-all: done."

