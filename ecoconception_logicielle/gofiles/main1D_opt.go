package main

import (
	"fmt"
	"math"
	"time"
)

func TDMAsolver(a, b, c, d, xc, tmp []float64) {
	var mc float64
	var dc, bc []float64
	var M int

	M = len(b)
    // use outputs for temporary arrays:
    bc = xc
    dc = tmp

	copy(bc, b)
	copy(dc, d)

	for it := 1; it < M; it++ {
		mc = a[it-1] / bc[it-1]
		bc[it] = bc[it] - mc * c[it - 1]
		dc[it] = dc[it] - mc * dc[it - 1]
	}
	bc[M-1] = dc[M-1] / bc[M-1]

	for it := M - 2; it > -1; it-- {
		bc[it] = (dc[it] - c[it] * bc[it + 1]) / bc[it]
	}
}

func RMS_E(X1, X2 []float64) float64 {
	var rms float64 = 0.0
	var M int
	M = len(X1)
	for it := 0; it < M; it++ {
		rms += (X1[it] - X2[it]) * (X1[it] - X2[it])
	}
	return math.Sqrt(rms / float64(M))
}

func solve(M int, dbg int) {
	var Lx, dx, dpdx, Re, Gammaa, dt, c0, Fe, De, Fw, eps, Dw, aE float64
	var aP, aW, last_rms, rms_u, min_step float64 //, rms_u, pi, last_rms, min_step float64
	var casee, bcnorth, bcsouth, maxiter, k int
        
	start := time.Now()

	var Aua, Auc []float64
	var Aub, Aud []float64
	var u, uold, tmp []float64

	Aua = make([]float64, M-1)
	Auc = make([]float64, M-1)
	Aub = make([]float64, M)
	Aud = make([]float64, M)

	uold = make([]float64, M)
	u    = make([]float64, M)
	tmp  = make([]float64, M)

	//     INPUT PARAMETERS
	//
	// Domain length
	Lx = 1.

	// Grid sizes
	dx = Lx / float64(M-1)

	// Pressure gradient
	dpdx = 1.0

	// Reynolds number: Re = U L / nu
	Re = 1.0

	// Diffusivity
	Gammaa = 1.0 / Re

	// Velocity
	c0 = 0.1

	// Time step:
	dt = 0.1

	// Configuration
	casee = 0 //'diffusion'
	//casee = 1 //'convection'

	// Pressure gradient
	dpdx = 1.0
	// Velocity
	c0 = 0.0
	// Diffusivity
	Gammaa = 1.0 / Re
	// Time step:
	dt = 0.1
	// Maximum number of iterations
	maxiter = 1000
	// Precision
	eps = 1e-9

	if dbg == 1 {
		fmt.Println("case =", casee)
		fmt.Println("M =", M)
		fmt.Println("dx =", dx)
		fmt.Println("dt =", dt)
		fmt.Println("eps =", eps)
	}

	//
	//     INITIALISATION
	//

    // boundary conditions
    bcnorth = 0
    bcsouth = 0

	// Convection fluxes
	//
	Fe = c0
	Fw = c0
	//
	// Diffusion fluxes
	//
	De = Gammaa / dx
	Dw = Gammaa / dx
	//
	// Compute system coefficients
	//
	aE = De + math.Max(-Fe, 0.0)
	aW = Dw + math.Max(Fw, 0.0)
	aP = aE + aW + Fe - Fw
	//
	// Assemble matrix coefficients
	//
	for it := 0; it < M-1; it++ {
		Aua[it] = -aW
		Auc[it] = -aE
	}
	for it := 1; it < M-1; it++ {
		Aub[it] = dx/dt + aP
	}
	// Min step check:
	last_rms = 1.0
	min_step = eps / 10.0

	// First iteration
	k = 0

	for ok := true; ok; ok = (k < maxiter) {
		//Predictor step (input : c0, Gamma, dx, dt, uold, dpdx; output : Aua, Aub, Auc, Aud)
		for it := 0; it < M; it++ {
			Aud[it] = dx / dt * uold[it] + dpdx * dx
		}
        // Boundary conditions
        // North boundary
        if bcnorth == 0 { // Dirichlet
            Aub[M - 1] = 1.0
            Aud[M - 1] = 0.0
        } else if bcnorth == 1 { // Neumann
            Aua[M - 2] = -1.0
            Aub[M - 1] = 1.0
            Aud[M - 1] = 0.0
        }
        // South boundary
        if bcsouth == 0 { // Dirichlet
            Aub[0] = 1.0
            Aud[0] = 0.0
        } else if bcsouth == 1 { // Neumann
            Auc[0] = -1.0
            Aub[0] = 1.0
            Aud[0] = 0.0
        }

        // Solve algebraic system
		TDMAsolver(Aua, Aub, Auc, Aud, u, tmp)

		// Compute rms error at the end of each time step
		rms_u = RMS_E(u, uold)

		if rms_u < eps {
			if dbg == 1 {
				fmt.Println("The simulation has converged in", k, " iterations")
			}
			break
		} else if math.Abs(last_rms-rms_u) < min_step {
			if dbg == 1 {
				fmt.Println("The simulation convergence is limited to ", rms_u, " in ", k, " iterations")
			}
			break
		} else {
			if dbg == 1 {
				fmt.Println("k= ", k, ": RMS U = ", rms_u)
			}
		}
		copy(uold, u)
		k += 1
		last_rms = rms_u
	}

	elapsed := time.Since(start)

    if dbg == 1 {
        fmt.Println("|Bench| Time (s): ", elapsed.Seconds())

        fmt.Println("result samples:")
        var step = M / 10
        for k = 0; k < M; k += step {
            fmt.Printf("u[%d] = %.17g\n", k, u[k])
        }
    }
}

func main() {
	var WI, WM, BI, BM int
	WI = 100
	WM = 64 * 1024
	BI = 10
	BM = 1 * 2048 * 2048

	fmt.Println("|Bench| WARMUP ", WI, " iterations [len = ", WM, "]---")
	now := time.Now()
	fmt.Println("|Bench| WARMUP Start: ", now.Unix())

	for it := 0; it < WI; it++ {
		solve(WM, 0)
	}

	now = time.Now()
	fmt.Println("|Bench| WARMUP Stop: ", now.Unix())

    time.Sleep(3 * time.Second)

	fmt.Println("|Bench| BENCHMARK ", BI, " iterations [", BM, "]---")
	now = time.Now()
	fmt.Println("|Bench| BENCHMARK Start: ", now.Unix())

	for it := 0; it < BI; it++ {
		solve(BM, 1)
	}

	now = time.Now()
	fmt.Println("|Bench| BENCHMARK Stop: ", now.Unix())
}
