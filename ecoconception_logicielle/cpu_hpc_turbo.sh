#!/bin/bash
# Needs sudo

# Use US Locale:
export LANG=C

echo "enable C-states ..."
cpupower -c all idle-set -E

# enable turbo-boost
echo 0 > /sys/devices/system/cpu/intel_pstate/no_turbo

echo "set CPU governor to performance ..."
cpupower -c all frequency-set -g performance 
cpupower -c all frequency-info

# set perf bias
cpupower -c all set -b 0
cpupower -c all info

