#!/bin/bash

# TRAP: Do not leave children jobs running if the shell has been cancelled
cleanup_trap() {
    CHILDREN_PIDS=$(jobs -p)
    if [ -n "$CHILDREN_PIDS" ]
    then
        trap - EXIT
        echo -e "SHELL cancelled, stopping $CHILDREN_PIDS"
        # we may try to send only TERM before a pause and a last loop with KILL signal ?
        kill $CHILDREN_PIDS

        echo -e "SHELL cancelled, waiting on $CHILDREN_PIDS"
        # wait for all pids
        for pid in $CHILDREN_PIDS; do
            wait $pid
        done

        CHILDREN_PIDS=$(jobs -p)
        if [ -n "$CHILDREN_PIDS" ]
        then
            echo -e "SHELL cancelled, killing $CHILDREN_PIDS"
            kill -9 $CHILDREN_PIDS
        fi
  fi
}
trap cleanup_trap EXIT


# HERE BEGINS THE SCRIPT

# Use US Locale:
export LANG=C

USE_CPU_POWER=1


if [ -z "$1" ]
  then
    echo "No argument 1 (CMD) supplied"
    exit 1
fi


processId="1D" # all benchmarks contains '1D'

# Check if service is running:
PID=`ps -e | grep "${processId}" | grep -v "grep" | grep -v "\-gov\.sh" | awk '{print $1}'`
#echo "PID: '${PID}'"

N_PID=`echo "${PID}" | wc -w`

if [ "${N_PID}" -ne "0" ]
then
    echo "Process '${processId}' (PID = ${PID}) is already running ..."
    exit 1
fi


# Initialize CPU:
if [ "$USE_CPU_POWER" -eq "1" ]
then
    echo "adjusting CPU (HT OFF) - requires SUDO"
    sudo ./cpu_ht_off.sh 2>&1 > /dev/null
fi


# Log host env:
./log_env.sh


# Log timestamps (needed by getwatts)
hostname > results/timestamp


CMD="$1"
DIR=`dirname $1`
LABEL=`basename $1`
HOSTNAME=`hostname`

dir=./results/${HOSTNAME}-$(date +"%Y_%m_%d")-gov/${LABEL}
mkdir -p $dir
echo "Store results in $dir"

logExt=txt
vmFile=${dir}/vm_${LABEL}.${logExt}



echo "---------------------------------------"
echo "Run benchmarks on command '$CMD' on 1 CPU core (cpu governors)"
echo "---"


# PROFILE in [cpu_normal | cpu_fixed | cpu_hpc | cpu_hpc_turbo]
for PROFILE in cpu_normal cpu_fixed cpu_hpc cpu_hpc_turbo
do
    # Initialize CPU:
    echo "adjusting CPU (${PROFILE}) - requires SUDO"
    sudo ./${PROFILE}.sh 2>&1 > results/cpu_profile.txt


    # Single job:
    echo "---"
    echo "Run Benchmark on 1 CPU (monothread) ... '$CMD'"

    logFile=${dir}/result_${PROFILE}_${LABEL}

    # Log timestamps (needed by getwatts)
    # Start monothread:
    date +%s >> results/timestamp
    sleep 2

    echo "Run process (CPU 0)..."
    taskset -c 0 $CMD > ${logFile}_single.${logExt} 2>&1

    echo "Results (monothread):"
    grep -h "|Bench|" ${logFile}_single.${logExt}

    echo "---"
done
   


# Log timestamps (needed by getwatts)
# End:
sleep 5
date +%s >> results/timestamp



# Restore CPU:
if [ "$USE_CPU_POWER" -eq "1" ]
then
    echo "adjusting CPU (HT ON + normal) - requires SUDO"
    sudo ./cpu_ht_on.sh 2>&1 > /dev/null
    sudo ./cpu_normal.sh 2>&1 > /dev/null
fi

trap - EXIT
echo "Done."
echo "---------------------------------------"

