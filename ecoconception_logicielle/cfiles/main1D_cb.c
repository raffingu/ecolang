/*
EcoInfo - Time & energy efficiency of programming languages in HPC
Copyright (C) 2020, CNRS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>


double* TDMAsolver(int M, double a[], double b[], double c[], double d[])
{
    /* Tri Diagonal Matrix Algorithm(a.k.a Thomas algorithm) solver
     TDMA solver, a b c d can be NumPy array type or Python list type.
     refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm

        Sub TriDiagonal_Matrix_Algorithm(N%, A#(), B#(), C#(), D#(), X#())
            Dim i%, W#
            For i = 2 To N
                W = A(i) / B(i - 1)
                B(i) = B(i) - W * C(i - 1)
                D(i) = D(i) - W * D(i - 1)
            Next i
            X(N) = D(N) / B(N)
            For i = N - 1 To 1 Step -1
                X(i) = (D(i) - C(i) * X(i + 1)) / B(i)
            Next i
        End Sub

    See https://www.harrisgeospatial.com/docs/TRISOL.html
     */
    const int length = sizeof ( double ) * M;
    double* bc = NULL;
    double* dc = NULL;
    double mc;
    int it;

    // copy b and d arrays to left input arrays intact:
    bc = (double *) malloc( length );
    dc = (double *) malloc( length );
    memcpy(bc, b, length);
    memcpy(dc, d, length);

    for (it = 1; it < M; it++)
    {
        mc = a[it - 1] / bc[it - 1];
        bc[it] = bc[it] - mc *  c[it - 1];
        dc[it] = dc[it] - mc * dc[it - 1];
    }
    // bc stores the results x:
    bc[M - 1] = dc[M - 1] / bc[M - 1];

    for (it = M - 2; it >= 0; it--)
    {
        bc[it] = (dc[it] - c[it] * bc[it + 1]) / bc[it];
    }
    free(dc);
    // note: bc must be freed by caller
    return bc;
}

inline double RMS_E(int M, double X1[], double X2[])
{
    double rms = 0.0;
    for (int it = 0; it < M; it++)
    {
        rms += (X1[it] - X2[it]) * (X1[it] - X2[it]);
    }
    return sqrt(rms / M);
}

int solve(int M, int dbg)
{
    double Lx, dx, dpdx, Re, Gammaa, c0, dt, Fe, De, Fw, eps, Dw, aE, aW, aP;
    double rms_u, last_rms, min_step;
    int casee, bcnorth, bcsouth, maxiter, k, it;
    double* Aua;
    double* Aub;
    double* Auc;
    double* Aud;
    double* u;
    double* uold;
    const int lengthM = sizeof ( double ) * M;
    const int lengthMm1 = sizeof ( double ) * (M - 1);
    struct timeval start, stop;
    double elapsed;
    
    gettimeofday(&start, NULL);


    //     INPUT PARAMETERS
    //
    // Domain length
    Lx = 1.0;

    // Grid sizes
    dx = Lx / (M - 1);

    // Pressure gradient
    dpdx = 1.0;

    // Reynolds number: Re = U L / nu
    Re = 1.0;

    // Diffusivity
    Gammaa = 1.0 / Re;

    // Velocity
    c0 = 0.1;

    // Time step:
    dt = 0.1;

    // Configuration
    casee = 0; //'diffusion'
    //casee = 1 //'convection'

    //
    //     INITIALISATION
    //


    Aua = (double *) malloc( lengthMm1 );
    Auc = (double *) malloc( lengthMm1 );

    Aub = (double *) malloc( lengthM );
    Aud = (double *) malloc( lengthM );

    uold = (double *) malloc( lengthM );
    u    = NULL;

    if (casee == 0)
    {
        // Pressure gradient
        dpdx = 1.0;
        // Velocity
        c0 = 0.0;
        // Diffusivity
        Gammaa = 1.0 / Re;
        // Time step:
        dt = 0.1;
        // Maximum number of iterations
        maxiter = 1000;
    } else {
        printf("Invalid case = %d \n", casee);
        return 1; // failure
    }

    // Precision
    eps = 1e-9;

    //
    // Print initial conditions:
    if (dbg == 1) {
        printf("case = %d \n", casee);
        printf("M = %d \n", M);
        printf("dx = %.17g \n", dx);
        printf("dt = %.17g \n", dt);
        printf("eps = %.17g \n", eps);
    }


    // boundary conditions
    bcnorth = 0;
    bcsouth = 0;

    // Initialisation of variables
    memset(Aua, 0.0, M - 1);
    memset(Auc, 0.0, M - 1);
    memset(Aub, 0.0, M);
    memset(Aud, 0.0, M);
    memset(uold, 0.0, M);

    //
    // Convection fluxes
    //
    Fe = c0;
    Fw = c0;
    //
    // Diffusion fluxes
    //
    De = Gammaa / dx;
    Dw = Gammaa / dx;
    //
    // Compute system coefficients
    //
    aE = De + fmax(-Fe, 0.0);
    aW = Dw + fmax( Fw, 0.0);
    aP = aE + aW + Fe - Fw;
    //
    // Assemble matrix coefficients
    //
    for (it = 0; it < M - 1; it++)
    {
        Aua[it] = -aW;
        Auc[it] = -aE;
    }
    for (it = 1; it < M - 1; it++)
    {
        Aub[it] = dx / dt + aP;
    }
    // Min step check:
    last_rms = 1.0;
    min_step = eps / 10.0;

    // First iteration
    k = 0;
    while (k < maxiter)
    {
        //Predictor step (input : c0, Gamma, dx, dt, uold, dpdx; output : Aua, Aub, Auc, Aud)
        for (it = 0; it < M; it++)
        {
            Aud[it] = dx / dt * uold[it] + dpdx * dx;
        }
        // Boundary conditions
        // North boundary
        if (bcnorth == 0)
        { // Dirichlet
            Aub[M - 1] = 1.0;
            Aud[M - 1] = 0.0;
        }
        else if (bcnorth == 1)
        { // Neumann
            Aua[M - 2] = -1.0;
            Aub[M - 1] = 1.0;
            Aud[M - 1] = 0.0;
        }
        // South boundary
        if (bcsouth == 0)
        { // Dirichlet
            Aub[0] = 1.0;
            Aud[0] = 0.0;
        }
        else if (bcsouth == 1)
        { // Neumann
            Auc[0] = -1.0;
            Aub[0] = 1.0;
            Aud[0] = 0.0;
        }

        // Solve algebraic system
        u = TDMAsolver(M, Aua, Aub, Auc, Aud);

        // Compute rms error at the end of each time step
        rms_u = RMS_E(M, u, uold);

        if (rms_u < eps)
        {
            if (dbg == 1) {
                printf("The simulation has converged in %d iterations\n", k);
            }
            break;
        }
        else if (fabs(last_rms - rms_u) < min_step)
        {
            if (dbg == 1) {
                printf("The simulation convergence is limited to %.17g in %d iterations\n", rms_u, k);
            }
            break;
        }
        else
        {
            if (dbg == 1) {
                printf("k=%d : RMS U = %.17g\n", k, rms_u);
            }
        }
        // copy values
        memcpy(uold, u, lengthM);
        free(u); u = NULL;
        k = k + 1;
        last_rms = rms_u;
    }

    gettimeofday(&stop, NULL);

    elapsed = (stop.tv_sec - start.tv_sec) + (stop.tv_usec - start.tv_usec) / 1e6;
    if (dbg == 1) {
        printf("|Bench| Time (s): %f \n", elapsed);

        printf("result samples:\n");
        for (k = 0; k < M; k += (M / 10))
        {
            printf("u[%d] = %.17g \n", k, u[k]);
        }
    }
    // free malloc'ed arrays !
    free(Aua); free(Aub); free(Auc); free(Aud);
    free(uold);
    return 0;
}

void test_solver()
{
    double aa[3] = {2.0, 2.0, 2.0}; // 0 at first
    double bb[4] = {-4.0, -4.0, -4.0, -4.0};
    double cc[3] = {1.0, 1.0, 1.0}; // 0 at end
    double rr[4] = {6.0, -8.0, -5.0, 8.0};
    double *res = TDMAsolver(4, aa, bb, cc, rr);
    double goodres[4] = {-1.0, 2.0, 2.0, -1.0};
    double rms_u = RMS_E(4, res, goodres);
    free(res);

    /*
    Math.ulp(1.0);
    ==> 2.220446049250313E-16
    */
    if (rms_u > 2E-16) {
      printf("ERROR SOLVER IS INEXACT: rms=%g\n",rms_u);
      exit(1);
    }
}

int main()
{
    const int WI = 100;
    const int WM = 64 * 1024;
    const int BI = 10;
    const int BM = 1 * 2048 * 2048;
    int i;
    struct timeval tv;

    test_solver();

    printf("|Bench| WARMUP %d iterations [len = %d]---\n", WI, WM);
    gettimeofday(&tv, NULL);
    printf("|Bench| WARMUP Start: %ld \n", tv.tv_sec);

    for (i = 0; i < WI; i++) {
        solve(WM, 0); /* hide logs */
    }

    gettimeofday(&tv, NULL);
    printf("|Bench| WARMUP WARMUP Stop: %ld \n", tv.tv_sec);

    sleep(3);

    printf("|Bench| BENCHMARK %d iterations [%d]---\n", BI, BM);
    gettimeofday(&tv, NULL);
    printf("|Bench| BENCHMARK Start: %ld \n", tv.tv_sec);

    for (i = 0; i < BI; i++) {
        solve(BM, 1); /* show logs */
    }

    gettimeofday(&tv, NULL);
    printf("|Bench| BENCHMARK Stop: %ld \n", tv.tv_sec);

    return 0;
}

