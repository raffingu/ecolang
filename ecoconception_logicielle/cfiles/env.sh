#!/bin/bash

CC=gcc
CCLAGS="-Wall -O3"
# Use native instructions for local machine's cpu architecture:
#CCLAGS="-Wall -O3 -march=native"

echo "CC version:"
$CC --version

echo "CCLAGS : $CCLAGS"

