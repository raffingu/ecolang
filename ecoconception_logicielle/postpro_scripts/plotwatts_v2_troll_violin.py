import process_data

show_plot=False

dir='../results/troll-1.grenoble.grid5000.fr-2022_01_09/'
host_name="troll1"
cpu_type="Xeon5218"
nTh=32
use_rust_opt3=False

# Power sampling:
pwr_dt=1.0 # 01 value(s) per second

# violin plot (max time)
time_ylim_1T=5.0
time_ylim_MT=8.0

process_data.process(dir, host_name, cpu_type, nTh,
            pwr_dt=pwr_dt,
            show_plot=show_plot, use_rust_opt3=use_rust_opt3,
            time_ylim_1T=time_ylim_1T, time_ylim_MT=time_ylim_MT)

