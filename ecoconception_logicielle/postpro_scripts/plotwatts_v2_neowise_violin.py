import process_data

show_plot=False

dir='../results/neowise-10.lyon.grid5000.fr-2022_01_06/'
host_name="neowise10"
cpu_type="amd"
nTh=48
use_rust_opt3=False

# Power sampling:
pwr_dt=1.0 # 01 value(s) per second

# violin plot (max time)
time_ylim_1T=4.0
time_ylim_MT=11.0

process_data.process(dir, host_name, cpu_type, nTh,
            pwr_dt=pwr_dt,
            show_plot=show_plot, use_rust_opt3=use_rust_opt3,
            time_ylim_1T=time_ylim_1T, time_ylim_MT=time_ylim_MT)

