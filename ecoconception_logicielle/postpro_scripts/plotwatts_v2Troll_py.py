import os
import numpy as np
import matplotlib.pyplot as plt

dir='../results/troll-1.grenoble.grid5000.fr-2022_01_09/'
host_name="troll1"
cpu_type="Xeon5218"
nTh=32

# Get watt data:
pwr_dt=1 #0.02 # 50 value(s) per second

with open(dir + '/powerW.out', 'rb') as f:
    timeTotal = np.load(f)

print("timeTotal dims: ", np.shape(timeTotal))
# timeTotal[timestamp][0 = Watt, 1 = Power integrated]


filelist = list()
filelist.append('1Dpython_numpy/result_1Dpython_numpy_single.txt')
filelist.append('1Dpython_numpy/result_1Dpython_numpy_core_2.txt')
filelist.append('1Dpython_numba/result_1Dpython_numba_single.txt')
filelist.append('1Dpython_numba/result_1Dpython_numba_core_2.txt')
filelist.append('1Dpython_transonic/result_1Dpython_transonic_single.txt')
filelist.append('1Dpython_transonic/result_1Dpython_transonic_core_2.txt')
filelist.append('1Dpython_numpy_opt/result_1Dpython_numpy_opt_single.txt')
filelist.append('1Dpython_numpy_opt/result_1Dpython_numpy_opt_core_2.txt')
filelist.append('1Dpython_numba_opt/result_1Dpython_numba_opt_single.txt')
filelist.append('1Dpython_numba_opt/result_1Dpython_numba_opt_core_2.txt')
filelist.append('1Dpython_transonic_opt/result_1Dpython_transonic_opt_single.txt')
filelist.append('1Dpython_transonic_opt/result_1Dpython_transonic_opt_core_2.txt')

print(filelist)

lablist1 = list()
lablist1.append('numpy')
lablist1.append('numba')
lablist1.append('transonic')
lablist1.append('numpy-opt')
lablist1.append('numba-opt')
lablist1.append('transonic-opt')

lablist = list()
for ii, label1 in enumerate(lablist1):
    lablist.append(f"{label1} * 1 process")
    lablist.append(f"{label1} * {nTh} process")

results = list()

for ii, filename in enumerate(filelist):
    case_1T = (ii%2 == 0)

    timings = np.zeros(10)
    n = 0

    with open(dir + filename) as f:
        for i, line in enumerate(f):
            if "BENCHMARK Start" in line:
                start = int(str.split(line, ':')[-1])
            if "BENCHMARK Stop" in line:
                stop = int(str.split(line, ':')[-1])
            if "|Bench| Time" in line:
                # parse indidual times ? '|Bench| Time (s):'
                timings[n] = float(str.split(line, ':')[-1])
                n = n + 1

    # print(f"timings: {timings}")
    # print(f"timings: mean = {np.mean(timings)} std = {np.std(timings)} median = {np.percentile(timings, 50)}, pct(95%) = {np.percentile(timings, 95)}")
    # Median / 95% percentile of iterations:
    ind_time_min = np.min(timings)
    ind_time_med = np.percentile(timings, 50)
    ind_time_pct95 = np.percentile(timings, 95)

    istart=np.where(timeTotal[:, 0] <= start)[-1][-1]
    istop =np.where(timeTotal[:, 0] >  stop)[0][0]
    # print("# ", case_1T, ii, filename, " time: ", istart, "to ", istop)

    rel_time = timeTotal[istart:istop, 0] - timeTotal[istart, 0]
    wall_time = timeTotal[istop, 0] - timeTotal[istart, 0]
#    nt = 1/nTh if case_1T else 1
    nt = 1 if case_1T else nTh
    power_inst = timeTotal[istart:istop, 1]
    power_inst_avg = np.mean(power_inst)
    power_integrated = np.sum(power_inst) * pwr_dt / 3600.0  # W.h
    power_reduced = power_integrated / nt

    results.append({
        'cpu_type': cpu_type, 'lang': lablist1[ii//2], 'nt': nt, 
        'ind_time_min': ind_time_min, 'ind_time_med': ind_time_med, 'ind_time_pct95': ind_time_pct95,
        'power_inst_avg': power_inst_avg, 'power_integrated': power_integrated, 'wall_time': wall_time/nt, 'power_reduced': power_reduced, 
        'filename': filename
    })

    plt.figure(99)
    plt.plot(rel_time, power_inst, label=lablist[ii])
    plt.grid(True)
    plt.legend()

    if case_1T:
        # Single Core (1T):
        plt.figure(100)
        plt.plot(rel_time, power_inst, label=lablist[ii])
        plt.grid(True)
        plt.legend()
    else:
        # Multi core (MT):
        plt.figure(101)
        plt.plot(rel_time, power_inst, label=lablist[ii])
        plt.grid(True)
        plt.legend()


# Post process (all results):
# group results:
results_noopt_1T = list()
results_noopt_MT = list()
results_opt_1T = list()
results_opt_MT = list()

# rank ie ratio (lang / C-opt):
ref_1T=results[2]
ref_MT=results[3]

# 1T
for ii, result in enumerate(results):
    case_1T = (ii%2 == 0)

    ref = ref_1T if case_1T else ref_MT

    result['score_ind_time_pct95'] = result['ind_time_pct95'] / ref['ind_time_pct95']
    result['score_wall_time'] = result['wall_time'] / ref['wall_time']
    result['score_power_reduced'] = result['power_reduced'] / ref['power_reduced']

    # group results:
    case_opt = ('opt' in result['lang'])
    if case_1T:
        if case_opt:
            results_opt_1T.append(result)
        else:
            results_noopt_1T.append(result)
    else:
        if case_opt:
            results_opt_MT.append(result)
        else:
            results_noopt_MT.append(result)

#print(results)


# Dump CSV Table
print("###################")
print(f"# host: {host_name}")
print("# CPU; Lang; Th; Inst. Power; Int. Power; Ind. time; Wall time; Red. Power; I.T. ratio; W.T ratio; P. ratio; filename")
print("# ;;;W;W.h;s;s;W.h / job;;;;")
# REF + 1T:
for ii, result in enumerate(results_noopt_1T):        
    print("{};{};{};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{}".format(result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['power_integrated'], result['ind_time_pct95'], result['wall_time'], result['power_reduced'], result['score_ind_time_pct95'],  result['score_wall_time'], result['score_power_reduced'], result['filename']))
# REF + MT:
for ii, result in enumerate(results_noopt_MT):        
    print("{};{};{};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{}".format(result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['power_integrated'], result['ind_time_pct95'], result['wall_time'], result['power_reduced'], result['score_ind_time_pct95'],  result['score_wall_time'], result['score_power_reduced'], result['filename']))
# REF + 1T:
for ii, result in enumerate(results_opt_1T):        
    print("{};{};{};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{}".format(result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['power_integrated'], result['ind_time_pct95'], result['wall_time'], result['power_reduced'], result['score_ind_time_pct95'],  result['score_wall_time'], result['score_power_reduced'], result['filename']))
# REF + MT:
for ii, result in enumerate(results_opt_MT):        
    print("{};{};{};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};{}".format(result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['power_integrated'], result['ind_time_pct95'], result['wall_time'], result['power_reduced'], result['score_ind_time_pct95'],  result['score_wall_time'], result['score_power_reduced'], result['filename']))


# Dump Latex Table
print("###################")
print(
"""

\\begin{tabular}{|l|c|l|}
\\hline
Column & Unit & Comments \\\\
\\hline
Inst. Power & W & average value over the job \\\\
Integrated Power & W.h & integrated power (50 Hz) of the job \\\\
Ind. time (I.T) & s & individual time (95\\% percentile) \\\\
Wall time & s & 1 job = 10 repeats \\\\
Red. Power & W.h & reduced power = Integrated Power / jobs \\\\
Ind. time ratio (I.T) & & individual time compared to (C-opt) \\\\
Wall Time ratio (W.T) & & Wall time compared to (C-opt) \\\\
Power ratio (P.) & & Power reduced compared to (C-opt) \\\\
\\hline
\\end{tabular}

""")

print("\\begin{tabular}{|c|l|c|c|c|c|c|c|c|c|c|}")
print(f"% host: {host_name}")
print("\\hline")
print("       CPU & Lang & Th & Inst. Power & Int. Power & Ind. time & Wall time & Red. Power & I.T. ratio & W.T ratio & P. ratio \\\\ % filename")
print("\\hline")
# REF + 1T:
for ii, result in enumerate(results_noopt_1T):        
    print("        {} & {} & {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\\\ % {}".format(result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['power_integrated'], result['ind_time_pct95'], result['wall_time'], result['power_reduced'], result['score_ind_time_pct95'],  result['score_wall_time'], result['score_power_reduced'], result['filename']))
print("\\hline")
# REF + MT:
for ii, result in enumerate(results_noopt_MT):        
    print("        {} & {} & {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\\\ % {}".format(result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['power_integrated'], result['ind_time_pct95'], result['wall_time'], result['power_reduced'], result['score_ind_time_pct95'],  result['score_wall_time'], result['score_power_reduced'], result['filename']))
print("\\hline \\hline")
# REF + 1T:
for ii, result in enumerate(results_opt_1T):        
    print("        {} & {} & {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\\\ % {}".format(result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['power_integrated'], result['ind_time_pct95'], result['wall_time'], result['power_reduced'], result['score_ind_time_pct95'],  result['score_wall_time'], result['score_power_reduced'], result['filename']))
print("\\hline")
# REF + MT:
for ii, result in enumerate(results_opt_MT):        
    print("        {} & {} & {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\\\ % {}".format(result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['power_integrated'], result['ind_time_pct95'], result['wall_time'], result['power_reduced'], result['score_ind_time_pct95'],  result['score_wall_time'], result['score_power_reduced'], result['filename']))
print("\\hline")
print("\end{tabular}")


plt.show()

fig, ax = plt.subplots()
labels = list()
for ii, result in enumerate(results_noopt_1T):
    if ii == 0:
        ax.bar(ii - 0.3, result['power_reduced'], color = 'b', width = 0.2, label = "Not optimised")
    else:
        ax.bar(ii - 0.3, result['power_reduced'], color = 'b', width = 0.2)#, label = "Not optimised")
    labels.append(result['lang'])
for iii, result in enumerate(results_opt_1T):
    if iii == 0:
        ax.bar(iii - 0.1, result['power_reduced'], color = 'g', width = 0.2, label = "Optimised")
    else:
        ax.bar(iii - 0.1, result['power_reduced'], color = 'g', width = 0.2)#, label = "Optimised")
for ii, result in enumerate(results_noopt_MT):
    if ii == 0:
        ax.bar(ii + 0.1, result['power_reduced'], color = 'r', width = 0.2, label = "Not optimised multithread")
    else:
        ax.bar(ii + 0.1, result['power_reduced'], color = 'r', width = 0.2)#, label = "Not optimised")
for iii, result in enumerate(results_opt_MT):
    if iii == 0:
        ax.bar(iii + 0.3, result['power_reduced'], color = 'k', width = 0.2, label = "Optimised multithread")
    else:
        ax.bar(iii + 0.3, result['power_reduced'], color = 'k', width = 0.2)#, label = "Optimised")
ax.set_ylabel('Power reduced')
ax.set_title('Power reduced by language')
ax.set_xticks(np.arange(ii+1))
ax.set_xticklabels(labels)
ax.legend()
fig.tight_layout()
plt.grid(True)
plt.show()

fig, ax = plt.subplots()
labels = list()
for ii, result in enumerate(results_noopt_1T):
    if result['lang'] == "numpy" or result['lang'] == "numpy-opt":
        result['power_reduced']=result['power_reduced']*10
    if ii == 0:
        ax.bar(ii - 0.1, result['power_reduced'], color = 'b', width = 0.2, label = "Not optimised monothread")
    else:
        ax.bar(ii - 0.1, result['power_reduced'], color = 'b', width = 0.2)#, label = "Not optimised")
    labels.append(result['lang'])
for iii, result in enumerate(results_opt_1T):
    if result['lang'] == "numpy" or result['lang'] == "numpy-opt":
        result['power_reduced']=result['power_reduced']*10
    if iii == 0:
        ax.bar(iii + 0.1, result['power_reduced'], color = 'g', width = 0.2, label = "Optimised monothread")
    else:
        ax.bar(iii + 0.1, result['power_reduced'], color = 'g', width = 0.2)#, label = "Optimised")
ax.set_ylabel('Energy (W.h)')
#ax.set_title('Power reduced by language')
ax.set_xticks(np.arange(ii+1))
ax.set_xticklabels(labels)
ax.legend()
fig.tight_layout()
plt.grid(True)
plt.show()

fig, ax = plt.subplots()
labels = list()
for ii, result in enumerate(results_noopt_MT):
    if result['lang'] == "numpy" or result['lang'] == "numpy-opt":
        result['power_reduced']=result['power_reduced']*10
    if ii == 0:
        ax.bar(ii - 0.1, result['power_reduced'], color = 'r', width = 0.2, label = "Not optimised multithread")
    else:
        ax.bar(ii - 0.1, result['power_reduced'], color = 'r', width = 0.2)#, label = "Not optimised")
    labels.append(result['lang'])
for iii, result in enumerate(results_opt_MT):
    if result['lang'] == "numpy" or result['lang'] == "numpy-opt":
        result['power_reduced']=result['power_reduced']*10
    if iii == 0:
        ax.bar(iii + 0.1, result['power_reduced'], color = 'k', width = 0.2, label = "Optimised multithread")
    else:
        ax.bar(iii + 0.1, result['power_reduced'], color = 'k', width = 0.2)#, label = "Optimised")
ax.set_ylabel('Energy (W.h)')
#ax.set_title('Energy by language')
ax.set_xticks(np.arange(ii+1))
ax.set_xticklabels(labels)
ax.legend()
fig.tight_layout()
plt.grid(True)
plt.show()

