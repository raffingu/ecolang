import process_data

show_plot=False

dir='../results/sagittaire-6.lyon.grid5000.fr-2022_01_06/'
host_name="sagittaire-6"
cpu_type="Opteron250"
nTh=2
use_rust_opt3=False

# Power sampling:
pwr_dt=1.0 # 01 value(s) per second

# violin plot (max time)
time_ylim_1T=15.0
time_ylim_MT=16.0

process_data.process(dir, host_name, cpu_type, nTh,
            pwr_dt=pwr_dt,
            show_plot=show_plot, use_rust_opt3=use_rust_opt3,
            time_ylim_1T=time_ylim_1T, time_ylim_MT=time_ylim_MT)

