#!/bin/bash

python3 plotwatts_v2_zb15_violin.py
python3 plotwatts_v2_rpi4_violin.py

python3 plotwatts_v2_troll_violin.py
python3 plotwatts_v2_neowise_violin.py
python3 plotwatts_v2_pyxis_violin.py
python3 plotwatts_v2_sagittaire_violin.py

