import numpy as np
import matplotlib.pyplot as plt
#fileout = "results/troll4_cpunor.out"
fileout = "results/troll4.out"
with open(fileout, 'rb') as f:
    timeC = np.load(f)
    timefortran = np.load(f)
    timejulia = np.load(f)
    timejuliaopt = np.load(f)
    timenumpy = np.load(f)
    timetranso = np.load(f)
    timenumba = np.load(f)

fileout = "results/troll4_cpufix.out"
fileout = "results/troll4_cpunor.out"
with open(fileout, 'rb') as f:
    timeCf = np.load(f)
plt.plot(timeC[:,0]-timeC[0,0], timeC[:,1])
plt.plot(timeCf[:,0]-timeCf[0,0], timeCf[:,1])
plt.grid()
plt.legend(['C normal', 'C fix'])
plt.show()


plt.plot(timeC[:,0]-timeC[0,0], timeC[:,1])
plt.plot(timefortran[:,0]-timefortran[0,0], timefortran[:,1])
plt.plot(timejulia[:,0]-timejulia[0,0], timejulia[:,1])
plt.plot(timejuliaopt[:,0]-timejuliaopt[0,0], timejuliaopt[:,1])
plt.plot(timenumpy[:,0]-timenumpy[0,0], timenumpy[:,1])
plt.plot(timetranso[:,0]-timetranso[0,0], timetranso[:,1])
plt.plot(timenumba[:,0]-timenumba[0,0], timenumba[:,1])
plt.grid()
#plt.legend(['C', 'jl', 'jl opt'])#, 'py np', 'transonic', 'numba'])
plt.legend(['C', 'fortran', 'jl', 'jl opt', 'py np', 'transonic', 'numba'])
plt.show()
