! 
! EcoInfo - Time & energy efficiency of programming languages in HPC
! Copyright (C) 2020, CNRS
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 2 of the License, or
! any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! 
module func
    integer, parameter :: dp = kind(1.0d0)
contains
    subroutine TDMAsolver(nf, a, b, c, d, xc, tmp)
        ! Tri Diagonal Matrix Algorithm(a.k.a Thomas algorithm) solver
        !TDMA solver, a b c d can be NumPy array type or Python list type.
        !refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
        ! 
        ! Sub TriDiagonal_Matrix_Algorithm(N%, A#(), B#(), C#(), D#(), X#())
        !     Dim i%, W#
        !     For i = 2 To N
        !         W = A(i) / B(i - 1)
        !         B(i) = B(i) - W * C(i - 1)
        !         D(i) = D(i) - W * D(i - 1)
        !     Next i
        !     X(N) = D(N) / B(N)
        !     For i = N - 1 To 1 Step -1
        !         X(i) = (D(i) - C(i) * X(i + 1)) / B(i)
        !     Next i
        ! End Sub
        ! 
        ! See https://www.harrisgeospatial.com/docs/TRISOL.html
        !
        implicit none
        integer, intent(in) :: nf
        real(kind = dp), dimension(nf - 1), intent(in) :: a, c
        real(kind = dp), dimension(nf), intent(in) :: b, d
        real(kind = dp), dimension(nf), intent(inout), TARGET :: xc, tmp
        real(kind = dp), POINTER :: bc(:), dc(:)
        integer :: it
        real(kind = dp) :: mc
 
        ! use outputs for temporary arrays:
        bc => xc
        dc => tmp

        ! copy b and d arrays to left input arrays intact:
        bc = b
        dc = d

        do it = 2, nf
            mc = a(it - 1) / bc(it - 1)
            bc(it) = bc(it) - mc *  c(it - 1)
            dc(it) = dc(it) - mc * dc(it - 1)
        enddo
        ! bc stores the results x:
        bc(nf) = dc(nf) / bc(nf)

        do it = nf - 1, 1, -1
            bc(it) = (dc(it) - c(it) * bc(it + 1)) / bc(it)
        enddo
    end subroutine

    function RMS_E(nf, X1, X2)
        implicit none
        integer, intent(in) :: nf
        real(kind = dp), dimension(nf), intent(in) :: X1, X2
        real(kind = dp) :: RMS_E
        RMS_E = sqrt(sum((X1 - X2)**2) / nf)
    end function

    function solve(M, dbg)
        integer, intent(in) :: M, dbg

        real(kind = dp) :: Lx, dx, dpdx, Re, Gammaa, c0, dt, Fe, De, Fw, eps, Dw, aE, aW, aP
        real(kind = dp) :: rms_u, last_rms, min_step
        integer :: casee, BCNORTH, BCSOUTH, maxiter, k
        real(kind = dp), allocatable :: Aua(:), Aub(:), Auc(:), Aud(:), u(:)
        real(kind = dp), allocatable :: uold(:), tmp(:)
        real(4) endtime, dtime, t(2)
        integer :: solve

        endtime = dtime(t)

        !     INPUT PARAMETERS
        !
        ! Domain length
        Lx = 1.0_dp

        ! Grid sizes
        dx = Lx / (M - 1)

        ! Pressure gradient
        dpdx = 1.0_dp

        ! Reynolds number: Re = U L / nu
        Re = 1.0_dp

        ! Diffusivity
        Gammaa = 1.0_dp / Re

        ! Velocity
        c0 = 0.1_dp

        ! Time step:
        dt = 0.1_dp

        ! Configuration
        casee = 0 !'diffusion'
        !casee = 1 !'convection'

        !
        !     INITIALISATION
        !


        allocate(Aua(M - 1), Auc(M - 1))
        allocate(Aub(M), Aud(M))
        allocate(u(M), uold(M), tmp(M))

        if (casee == 0) then
            ! Pressure gradient
            dpdx = 1.0_dp
            ! Velocity
            c0 = 0.0_dp
            ! Diffusivity
            Gammaa = 1.0_dp / Re
            ! Time step:
            dt = 0.1_dp
            ! Maximum number of iterations
            maxIter = 1000
        else
            write(*, *) 'Invalid case = ', casee
            solve = 1
            return ! failure
        end if

        ! Precision
        eps = 1e-9_dp

        !
        if (dbg == 1) then
        ! Print initial conditions:
        write(*, *) 'case : ', casee
        write(*, *) 'M  : ', M
        write(*, *) 'dx : ', dx
        write(*, *) 'dt : ', dt
        write(*, *) 'eps: ', eps
        end if

        ! boundary conditions
        BCNorth = 0
        BCSouth = 0

        ! Initialisation of variables
        Aua(:) = 0.0_dp
        Aub(:) = 0.0_dp
        Auc(:) = 0.0_dp
        Aud(:) = 0.0_dp

        uold(:) = 0.0_dp

        ! u, tmp could stay dirty (non zero fill):
        ! u(:) = 0.0_dp
        ! tmp(:) = 0.0_dp

        !
        ! Convection fluxes
        !
        Fe = c0
        Fw = c0
        !
        ! Diffusion fluxes
        !
        De = Gammaa / dx
        Dw = Gammaa / dx
        !
        ! Compute system coefficients
        !
        aE = De + max(-Fe, 0.0)
        aW = Dw + max(Fw, 0.0)
        aP = aE + aW + Fe - Fw
        !
        ! Assemble matrix coefficients
        !
        Aua(1:M - 1) = -aW
        Aub(2:M - 1) = dx / dt + aP
        Auc(1:M - 1) = -aE

        ! Min step check:
        last_rms = 1.0_dp
        min_step = eps / 10.0

        ! First iteration
        k = 0
        do while (k < maxIter)
            !Predictor step (input : c0, Gamma, dx, dt, uold, dpdx; output : Aua, Aub, Auc, Aud)
            Aud(:) = dx/dt * uold(:) + dpdx * dx

            ! Boundary conditions
            ! North boundary
            if (BCNorth == 0) then ! Dirichlet
                Aub(M) = 1_dp
                Aud(M) = 0_dp
            else if (BCNorth == 1) then ! Neumann
                Aua(M) = -1_dp
                Aub(M) = 1_dp
                Aud(M) = 0_dp
            end if
            ! South boundary
            if (BCSouth == 0) then ! Dirichlet
                Aub(1) = 1_dp
                Aud(1) = 0_dp
            else if (BCSouth == 1) then ! Neumann
                Auc(1) = -1_dp
                Aub(1) = 1_dp
                Aud(1) = 0_dp
            end if

            ! Solve algebraic system
            call TDMAsolver(M, Aua, Aub, Auc, Aud, u, tmp)

            ! Compute rms error at the end of each time step
            rms_u = RMS_E(M, u, uold)

            if (rms_u < eps) then
                if (dbg == 1) then
                    write(*, *) 'The simulation has converged in ', k, ' iterations'
                end if
                exit
            else if (abs(last_rms - rms_u) < min_step) then
                if (dbg == 1) then
                    write(*, *) 'The simulation convergence is limited to ', rms_u, ' in ', k, ' iterations'
                end if
                exit
            else
                if (dbg == 1) then
                    write(*, *) 'k=', k, ': RMS U =', rms_u
                end if
            end if

            ! copy values
            uold = u
            k = k + 1
            last_rms = rms_u
        end do

        endtime = dtime(t)
        if (dbg == 1) then
        write(*, *) '|Bench| Time (s): ', endtime

        write(*, *) 'result samples:'
        do k = 1, M, M/10
            write(*, *) 'u[', k, '] = ', u(k)
        enddo
        endif

        deallocate(Aua, Aub, Auc, Aud)
        deallocate(u, uold, tmp)
        solve = 0
    end function
end module

program undfor
    use func
    implicit none
    integer :: WI = 100
    integer:: WM = 64 * 1024
    integer :: BI = 10
    integer:: BM = 2048 * 2048
    integer :: i, sol
    integer :: tv

    write(*, *) '|Bench| WARMUP ', WI,' iterations [len = ', WM, ']---'
    tv = time()
    write(*, *) '|Bench| WARMUP Start: ', tv

    do i =1, WI
      sol = solve(WM, 0) ! hide logs
    enddo

    tv = time()
    write(*, *) '|Bench| WARMUP Stop: ', tv

    ! sleep (gcc extension)
    call sleep(3)

    write(*, *) '|Bench| BENCHMARK ', BI,' iterations [len = ', BM, ']---'
    tv = time()
    write(*, *) '|Bench| BENCHMARK Start: ', tv

    do i =1, BI
      sol = solve(BM, 1) ! show logs
    enddo

    tv = time()
    write(*, *) '|Bench| BENCHMARK Stop: ', tv

end program undfor
