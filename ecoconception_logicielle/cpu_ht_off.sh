#!/bin/bash
# Needs sudo

# Use US Locale:
export LANG=C

for i in $(cat /sys/devices/system/cpu/cpu*/topology/thread_siblings_list | awk -F',' '{print $2}' | sort -u); do 
    echo "Disabling logical HT core ${i}"
    echo 0 > /sys/devices/system/cpu/cpu${i}/online;
done

